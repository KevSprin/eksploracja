Europa League - Two Russian teams through

Kuban earned a good draw away in Spain at Valencia but it was not enough, while Rubin Kazan had an excellent 2-0 victory at Zulte-Waregem in Belgium, guaranteeing first place in the group. Anzhi Makhachkala was less fortunate, coming away from Tottenham Hotspur in London, England with a 4-1 thrashing. But Anzhi had done enough.
The first two teams in each group go through to the next round, where they will be joined by the following teams, which finished in third place in the Champions League:
Shakhtar Donetsk, SL Benfica, Plzen, Juventus, Basel, Napoli, FC Porto, Ajax.
Tottenham and Red Bull Salzburg are totalists: 6 games, six victories, 18 points
Group A 
Valencia 1 Kuban 1
St. Gallen 1 Swansea City 0
After 6 games
Valencia 13 points, Swansea 8, Kuban 6, St. Gallen 6
 
Group B
Dinamo Zagreb 1 Razgrad Ludogorets 2
PSV 0 Chornomorets Odessa 1
After 6 games
Razgrad 16, Chornomorets 10, PSV 7, D. Zagreb 1
 
Group C
Salzburg 3 Esbjerg 0
Standard Liege 1 Elfsborg 3
After 6 games
Salzburg 18, Esbjerg 12, Elfsborg 4, St. Liege 1
 
Group D
Zulte-Waregem 0 Rubin Kazan 2
Maribor 2 Wigan Athletic 1
After 6 games
Rubin 14, Maribor 7, Zulte-Waregem 7, Wigan 5
 
Group E
Fiorentina 2 Dnipro 1
Pandurii 0 Paços de Ferreira 0
After 6 games
Fiorentina 16, FC Dnipro 12, Paços de Ferreira 3, Pandurii 2
 
Group F
Eintracht Frankfurt 2 APOEL 0
Maccabi Tel-Aviv 1 Bordeaux 0
After 6 games
Eintracht Frankfurt 15, Maccabi Tel Aviv 11, Apoel Nicosia 5, Bordeaux 3
  
Group G
Thun 0 Genk 1
Dinamo Kyiv 3 Rapid Vienna1
After 6 games
Genk 14, Dynamo Kiev 10, Rapid Vienna 6, Thun 3
 
Group H
Freiburg 0 Sevilla 2
Estoril 1 Liberec 2
After 6 games
Sevilla 12, Liberec 9, Freiburg 6, Estoril 3
 
Group I
Betis 0 Rijeka 0
Guimarães 1 Lyon 2
After 6 games
Lyon 12, Betis 9, Guimarães 5, Rijeka 4
 
Group J
Lazio 0 Trabzonspor 0
Apollon 0 Legia 2
After 6 games
Trabzonspor 14, Lazio 12, Apollon 4, Legia 3
 
Group K
Tottenham 4 Anzhi Makhachkala 1
Sheriff  2 Tromso 0
After 6 games
Tottenham 18, Anzhi 8, Sheriff 6, Tromso 1 
 
Group L
PAOK 2 AZ Alkmaar 2
Maccabi Haifa 2 Shakhter Karagandy 1
After 6 games
AZ Alkmaar 12, PAOK 12, Maccabi Haifa 5, Shakhter 2 
 
  
The groups
Parte inferior do formulário
Group A
Swansea City (Wales), Kuban Krasnodar (RUS), St. Gallen (SWI), Valencia (SPA)
Group B
Dinamo Zagreb (CRO), Ludogorets Razgrad (BUL), PSV Eindhoven (NETHERLANDS), Chornomorets Odessa (UKR)
Group C
SV Salzburg (AUS), Esbjerg (DEN), Standard Liege (BEL), Elfsborg (SWE)
Group D
Rubin Kazan (RUS), Wigan (ENG), Zulte-Waregem (BEL), NK Maribor (SLO)
Group E
Dnipro Dnepropetrovsk (UKR), Fiorentina (ITA), Paços de Ferreira (POR), Pandurii Targu (ROM)
Group F
Eintracht Frankfurt (GER), Apoel Nicosia (CYP), Maccabi Tel-Aviv (ISR), Bordeaux (FRA)
Group G
Dynamo Kiev (UKR), Genk (BEL), Rapid Vienna (AUS), Thun (SWI)
Group H
Estoril Praia (POR), Liberec (CZE), SC Freiburg (GER), Sevilla (SPA)
Group I
Guimarães (POR), Lyon (FRA), Real Betis (SPA), Rijeka (CRO)
 Group J
Apollon Limassol (CYP), Lazio (ITA), Legia Warsawa (POL), Trabzonspor (TUR)
Group K
Anzhi Makhachkala (RUS), Sheriff Tiraspol (MOL), Tottenham Hotspur (ENG), Tromso (NOR)
Group L
AZ Alzmaar (NETHERLANDS), Maccabi Haifa (ISR), PAOK Salonika (GRE), Shakhter Karagandy (KAZ)
 
Next round: February 20 and 27
Timothy Bancroft-Hinchey
Pravda.Ru
 