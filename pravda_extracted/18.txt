Russia to export arms worth $50 billion annually by 2020

 
By 2020, Russia will be able to reach the level of $50 billion annually in arms exports, Deputy Prime Minister Dmitry Rogozin said.
Thus, Russian arms exports are expected to quadruple. According to Rogozin, there is no doubt that Russia will not only retain the status of the second largest exporter of arms, but also reduce the gap between first and second places.
In 2013, Rosoboronexport planned to export military hardware worth more than 13 billion dollars.
Meanwhile, a joint project between Russia and NATO on safe disposal of outdated ammunition in the Kaliningrad enclave will require 50 million euros for its implementation for the period of five years, NATO Director of Information Office in Moscow Robert Pszczel said. 
The agreement between Russia and NATO on the joint destruction of ammunition was reached in December in Brussels.
 