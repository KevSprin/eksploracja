There will be no Greenpeace between Putin and King of the Netherlands

Russian President Vladimir Putin and the King of the Netherlands do not intend to discuss questions related to the beating of diplomats and the action of Greenpeace, as it is not within the competence of the king. The main emphasis will be made on the cultural dimension of cooperation, presidential aide Yuri Ushakov said.
In recent times, a number of complications surfaced between Russia and the Netherlands due to an attack on a Russian diplomat in The Hague and the arrest of the crew of the Arctic Sunrise in Russia that was navigating under the Dutch flag.
The visit of the Dutch monarch to Russia is timed to end the years of Russia and the Netherlands in the two countries. 
 