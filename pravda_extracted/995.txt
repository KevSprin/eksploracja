Champions: Away wins for Barça and PSG

Man of the Match Daniel Alves and, who else? Messi, saw off Manchester City at the Etihad Stadium, while Paris Saint-Germain crossed over into Germany to come away from Leverkusen 4-0 winners. Matuidi and two from (who else?) Zlatan Ibrahimovic (Man of the Match) put the game beyond the hosts' reach. Cayabe added to the misery near the end.
 
Manchester City FC 0 FC Barcelona 2
                              Messi 54 (pen.); Daniel Alves 90
 
The sending-off of Demichelis on 53' was the beginning of the end for City. Messi scored from the spot one minute later and Daniel Alves made City's woes more intense on 90'. It was a game which Barcelona controlled with 62 per cent of ball possession against City's 38. There were 10 Barça shots to Manchester City's 9 and 4 corners to one.
 
Bayer 04 Leverkusen 0 Paris Saint-Germain 4
                                 Matuidi 3; Ibrahimovic 39 (pen.), 42; Cabaye 88
 
Man of the Match Ibrahimovic scored a brace after Matuidi started off the best way for the Parisians. By the time Cabaye had sent the match out of Bayer's reach, Spahic was went off on 59', in a game in which PSG had 12 shots, all on target, to Bayer's 5 (3 on) and the visitors had a ball possession of 59% to 41%.
  
Champions and Europa League draws
 
The matches are drawn for the last 16 clubs of the Champions League and the last 32 of the Europa League The Champions League matches will take place on February 18/19 and 25/26 and March 11/12 and 18/19. The Europa League games will be on February 20 and 27. Zenit is the only flag carrier for Russia in the Champions, 3 teams continue in Europa.
 
Champions League fixtures
 Tuesday 18 February & Wednesday 12 March: 
Manchester City FC 0 FC Barcelona 2
Bayer 04 Leverkusen 0 Paris Saint-Germain 4  Wednesday 19 February & Tuesday 11 March: AC Milan v Club Atlético de Madrid, Arsenal FC v FC Bayern München  Tuesday 25 February & Wednesday 19 March: Olympiacos FC v Manchester United FC, FC Zenit v Borussia Dortmund  Wednesday 26 February & Tuesday 18 March: Galatasaray AŞ v Chelsea FC, FC Schalke 04 v Real Madrid CF
 
England (4): Arsenal, Chelsea FC, Manchester City, Manchester United
Spain (3): Atlético de Madrid, FC Barcelona, Real Madrid
Germany (4): Bayer Leverkusen, Bayern Munchen, Borussia Dortmund, Schalke 04
France (1): Paris Saint-Germain
Italy (1): AC Milan
Greece (1): Olympiacos FC
Russia (1): Zenit Saint Petersburg
Turkey (1): Galatasaray AS
 
Europa League
 
Games to be played on February 20 and 27
 
Dnipro (UKR) v Tottenham (ENG)
Betis (ESP) v Rubin (RUS)
Swansea (English League - WALES) v Napoli (ITA)
Juventus (ITA) v Trabzonspor (TUR)
Maribor (SVN) v Sevilla (ESP)
Plzeň (CZE) v Shakhtar Donetsk (UKR)
Chornomorets Odesa (UKR) v Lyon (FRA)
Lazio (ITA) v Ludogorets (BUL)
Esbjerg (DEN) v Fiorentina (ITA)
Ajax (NED) v Salzburg (AUT)
M. Tel-Aviv (ISR) v Basel (SUI)
FC Porto (POR) v Eintracht Frankfurt (GER)
Anji (RUS) v Genk (BEL)
Dynamo Kyiv (UKR) v Valencia (ESP)
PAOK (GRE) v Benfica (POR)
Liberec (CZE) v AZ (NED)
 
Austria: Salzburg
Belgium: Genk
Bulgaria: Ludogorets
Czech Republic (2): Liberec, Plzeň
Denmark: Esbjerg
England: Tottenham Hotspur
English League, Wales: Swansea City FC
France: Lyon
Germany: Eintracht Frankfurt
Greece: PAOK
Italy (4): Fiorentina, Juventus, Lazio, Napoli
Israel: Maccabi Tel-Aviv
Netherlands (2): Ajax, AZ Alkmaar
Portugal (2): Benfica, FC Porto
Russia (2): Anzhi Makkachkala, Rubin Kazan,
Slovenia: Maribor
Spain (3): Betis, Sevilla, Valencia
Switzerland: Basel
Turkey: Trabzonspor
Ukraine (4): Chornomorets Odesa, Dnipro Dnipropetrovsk, Dinamo Kyiv, Shakhtar Donetsk
 
Total number of teams in European competitions:
Champions League + Europa League = Total
 
England: 4 + 1 = 5 + Swansea City = 6
Spain 3 + 3 = 6
Germany: 4 + 1 = 5
Italy : 1 + 4 = 5
Ukraine: 0 + 4 = 4
Russia: 1 + 2 = 3
Czech Republic: 0 + 2 = 2
France: 1 + 1 = 2
Greece: 1 + 1 = 2
Netherlands: 0 + 2 = 2
Portugal: 0 + 2 = 2
Turkey: 1 + 1 = 2
Austria: 0 + 1 = 1
Belgium: 0 + 1 = 1
Bulgaria: 0 + 1 = 1
Denmark: 0 + 1 = 1
English League, Wales: 1 Europa
Israel: 0 + 1 = 1
Slovenia: 0 + 1 = 1
Switzerland: 0 + 1 = 1
 
Timothy Bancroft-Hinchey
Pravda.Ru
 
 