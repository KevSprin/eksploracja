Putin named No. 1 politician in World Ranking 2013

Russian President Vladimir Putin has become number one politician in the "World Ranking 2013." As many as 175 news agencies and news media took part in the work on the ranking.
Vladimir Putin received 133 votes out of 175.
The top of the ranking also includes Pope Francis and German Chancellor Angela Merkel.
Among other 53 candidates, U.S. President Barack Obama, Chinese Vice President Xi Jinping and first black president of South Africa Nelson Mandela, who died in December last year, also received many voted, ITAR-TASS reports.
According to the agency, the survey was conducted with the participation of members of all existing international and regional associations of news agencies, including the European Alliance of News Agencies (EANA), the Organization of News Agencies in Asia and the Pacific (OANA), the Federation of Arab News Agencies (FANA) and the Association of National News Agencies of the Black Sea region (BSANNA).
 