Ukrainian protesters lose their eyes and hands in violent clashes

 
Forty-two protesters, who took part in violent clashes with police in Kiev's government quarters, were hospitalized with severe injuries. Three of them lost their eyes in the fighting, another man lost his hand, Director of the Department of Health of the Kiev city administration, Vitaly Mokhorev said.   Most of the injured suffered injuries of their eyes. Vitaly Mokhorev could neither confirm nor deny the information that the police used rubber bullets to disperse demonstrators.
 
"We can only say that it was eye contusion, and it is up to physicians to establish the causes.
 Most of those who were hospitalized are Kiev residents, but there are people from other Ukrainian regions too.
 Opposition activists were reportedly armed with two-meter-long sticks. Aggressive protesters burned several police buses and cars. They also threw boulders at riot police. About a hundred riot police officers were injured, 60 of them were hospitalized.
 
 