White House terminates preparations to G8 summit in Russia's Sochi

 
In connection with the current events in Ukraine, the press service of the U.S. White House announced the suspension of preparations for the G8 summit in Sochi (Canada, France, Germany, Italy, Japan, United Kingdom and United States). The summit has been postponed until "the situation returns to the point, where the G8 can start a discussion."
Representatives of the G7 countries also declared their support for the sovereignty and territorial integrity of Ukraine. They also said that the troubled country needs the support from the International Monetary Fund. "The IMF support will be critical in order to unlock additional assistance from the World Bank and other international financial institutions, the EU and bilateral sources," officials said.
 