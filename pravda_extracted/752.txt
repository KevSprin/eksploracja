Cristiano Ronaldo on fire

Another hat-trick from Cristiano Ronaldo, who leads La Liga and the Champions League lists of goalscorers (16 and 8, respectively), his third hat-trick of the season to add to his five braces. Cristiano Ronaldo, Real Madrid's star forward, is on fire. In Moscow, Spartak's Yura Movsisyan (photo) sinks Zenit.
La Liga (After 13 matches)
Barcelona (37) beat Betis away 4-1, Neymar chalking up what cound be a contender for goal of the season, Messi coming off injured. Three points behind, Atlético Madrid, held 1-1 at Villarreal, while on 31, Cristiano Ronaldo's Real Madrid thrashed Real Sociedad 5-1.
Premiership (After 11 matches)
Arsenal is still top on 25 points despite the 1-0 away defeat at Manchester United. Liverpool took advantage to go 2 points behind, beating Fulham 4-0, while Southampton move up to third on 22 points with a 4-1 demolition of Hull City. In fourth is Chelsea (21), held at home 2-2 by West Bromwich Albion. Three teams follow on 20 points: Manchester United, ominously, Everton and Tottenham.
Bundesliga (After 12 matches)
Bayern Munich (32) beat Augsburg and is four points clear of Borussia Dortmund (2-1 loser at in-form Wolfsburg) and Leverkusen (5-3 victor against Hamburg).
Serie A (After 12 matches) 
Of the top three, Juventus took full advantage, beating third-placed Napoli 3-0 in Turin, while Roma was held 1-1 at home by Sassuolo. Roma is first with 32 points, Juventus has 31 and SCC Napoli has 28.
Ligue 1 (After 13 matches)
PSG (31) guarantees a fortnight´s stay at the top of Ligue 1, with a 3-1 win over Nice in Paris, while rivals Lille (27) and Monaco (26) dropped points at Guingamp (0-0= and at home to Évian (1-1).
Primeira Liga (After 9 matches)
Cup weekend in Portugal, where FC Porto won 2-0 at Guimarães, while Benfica pipped Sporting 4-3 after extra time in a fantastic match of football at the Estádio da Luz. In the championship, FC Porto is first on 23 points, Sporting Clube de Portugal (20) is second and Benfica (20) third.
Russian Premier League (After 16 matches)
Lokomotiv and Spartak are hot on the heels of Zenit, the leader (36 points), three points behind after Spartak beat Zenit in Moscow 4-2 with a hat-trick from Yura Movsisyan, while Lokomotiv went down 2-0 at Tom. CSKA Moskva (fourth on 30) beat Terek Grozny 4-1 in Moscow while fifth-placed Dinamo Moskva (29) won 2-1 at Krylya Sovetov.
Fenerbahçe leads the Turkish league by four points (28) over Kasimpasa SK after all teams have played 11 games.
In Armenia FC Ararat leads with 24 points from 12 games; in Azerbaijan, Qarabag and Nefti have 28 points from 13 games; in Belarus after 30 games, BATE Borisov has pulled ten points clear (64) and celebrates the eighth successive title with two games to go.
After 27 games in Kazakhstan, FC Aktobe has 43 points from 32 games and is 1 point ahead of FC Astana.
In Ukraine after 16 games, the top spot belongs to Shakhtar Donetsk (35 points), one point ahead of Metalist Kharkiv (34 points) but Kharkiv has a game in hand.
Timothy Bancroft-Hinchey
Pravda.Ru
 