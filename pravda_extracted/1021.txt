First three Greenpeace activists released

 
Russia's Investigative Committee pardoned the first three members of the Arctic Sunrise crew, news agencies reported citing a source at Greenpeace. 
Earlier, Greenpeace lawyer Andrey Suchkov reported that the criminal proceedings against one or two environmentalists would be terminated on December 24th. All other cases were to be closed in the next couple of days, the lawyer said. After the criminal cases are closed, the foreign environmentalists will be able to leave Russia.
On November 19th, it was announced that three foreign Greenpeace activists, as well as three Russians from the Arctic Sunrise vessel, were released on bail.
Thirty activists (including 26 foreigners) were detained September 19 after they conducted an action of protest against the development of the Arctic region. The activists attacked Russia's Prirazlomnaya oilrig, but were arrested. In early October, the detainees were accused of piracy, but the case was later reclassified as "hooliganism."
 