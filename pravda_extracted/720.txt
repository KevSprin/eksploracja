Mourinho's magic

Chelsea FC looks menacing, the London club has the hallmark of Mourinho all over it, with the sectors working well together, exploring opportunities and tellingly, running off the ball to find positions. Has Arsenal the strength in depth to keep top spot for the rest of the season ahead of free-scoring City? Have Arsenal's owners the commitment?
Premiership (After 22 matches)
Wins for the top three in the Premier League, Arsenal (51) sinking Fulham 2-0 with a brace of goals from Santi Cazorla, Manchester City (50) thumping four past Carfiff (4-2), goals from Dzeko, Jesús Navas, Y. Touré and Aguero; Chelsea (49) added to Manchester United's miserable Fergusonless season 3-1 with a hat-trick from Samuel Eto'o.
 La Liga (After 20 matches)
Real took advantage of a second week of draws for Barcelona (51), 1-1 at Levante and Atlético (51), held 1-1 at home by Sevilla by thrashing Betis away 5-0 with goals from Cristiano Ronaldo, Bale, Benzema, Di Maria and Morata.
Bundesliga (After 17 matches)
FC Bayern Munchen lead on 44 points with 16 games. Bayer Leverkusen is second with 37 points from 17 games and VfL Borussia Moenchengladbach is third on 33 points (17 games). The next round starts on January 24.
Serie A (After 20 matches) 
Juventus looks more and more to have Serie A sewn up with the two Milan clubs virtually invisible at the top of the Italian league. Juve (55) beat Sampdoria 4-2; AS Roma (47) beat Livorno 3-0 while SSC Napoli could only manage a draw at Bologna (2-2) - Bianchi got his second for the home team on 90'.
 Ligue 1 (After 21 matches)
Paris Saint-Germain and Monaco are left in the race in France. PSG (50) thrashed Nantes 5-0, with goals from Thiago Silva, a brace from Ibrahimovic, Thiago Motta and Cavani. AS Monaco (45) beat Toulouse away 2-0, third-placed LOSC Lille (40) lost 2-0 at Saint-Étienne
 Primeira Liga (After 16 matches)
Benfica, Sporting and Porto lead the Portuguese League. Benfica (39) beat Marítimo (Madeira) 2-9 with goals from Rodrigo, Sporting CP (37) won 2-1 at Arouca and FC Porto (36) beat Setúbal easily, 3-0 after the defeat at Benfica last week, a game in which the refereeing raised many eyebrows.
 Russian Premier League (After 19 matches)
The Russian Premier is hot, hot, hot. Zenit recovered with a 2-1 win over Ural and is equal first (40 points) with Lokomotiv (drawless home game with Rubin). Spartak Moskva is third with 39 points (1-0 winner at Rostov).  Dinamo Moskva (35) beat Amkar 2-0 in Moscow, while CSKA (34) lost 1-0 at Krasnodar. The championship resumes in March.
Fenerbahçe leads the Turkish league increasing the gap to eight points (41) over Galatasaray AS after 17 games. The championship resumes on January 24.
In Armenia FC Ararat leads, 8 points clear, with 30 points from 14 games. The Championship resumes in March; in Azerbaijan, Qarabag leads with 36 points from 18 games, one ahead of Qäbälä. The championship resumes at the end of January; in Belarus after 32 games, BATE Borisov is the Champion. In Kazakhstan, FC Aktobe is champion.
In Ukraine after 18 games, the top spot belongs to Shakhtar Donetsk (41 points), five points ahead of Dynamo Kyiv. Metalist Kharkiv (36) and Dnipro Dnipropetrovsk (35) follow in third and fourth place, both with a game in hand. The championship resumes in March.
Timothy Bancroft-Hinchey
Pravda.Ru
 