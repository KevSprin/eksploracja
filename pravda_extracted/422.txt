Greenpeace activists given green light to leave Russia

 
Greenpeace environmentalists may receive visas to leave Russia and return to their homelands already this week, Greenpeace lawyer Michael Krendlin said.
According Kreindlin, the activists may fly home this week, but one could not guarantee that. It depends on the Investigative Committee and the Federal Migration Service of the Russian Federation to make the final decision. As soon as the Investigative Committee orders to dismiss the case, the Federal Migration Service will allow their departure from Russia, said the lawyer.
Kreindlin also said that arrest would also be lifted from the Arctic Sunrise vessel in Murmansk.
In September 2013, Greenpeace activists tried to climb onto Prirazlomnaja oilrig in the Pechora Sea to protest against the extraction of hydrocarbons in the Arctic. Thirty members of the ship were arrested.
 