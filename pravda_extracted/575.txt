Pope Francis lifts the veil of his past

 
Pope Francis admitted that he worked as a bouncer at a nightclub in Argentina, CNN reports. Journalists joked that if St. Peter would ever need an assistant at the gates of heaven, Pope Francis would be an ideal candidate for this role.In addition, the Pope said that in his youth he worked as a janitor and was employed at a chemical laboratory, where he liked to conduct experiments. The Pope noted that all this was long before he felt the ability to draw people to the church.The Pontiff said that his confessions would not hurt his reputation, but rather enhance the perception of him as " the Pope of the people."
 