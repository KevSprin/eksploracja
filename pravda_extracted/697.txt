Student armed with guns takes 20 children hostage in Moscow

 
A man armed with a gun broke into the building of School No. 263 in the north of Moscow. He wounded two policemen, who were on duty at school, and took at least 20 students hostage.
The attackers proved to be a student of the same school. Most of the students were evacuated from the building immediately. Others were released after the attacker was neutralized.
Nothing has been said about the motives that the man had. He was identified as Sergei Gordeev, a senior student. Reportedly, the young man had a personal conflict at school. It was the man's father, who helped to detain him.  
No children were hurt in the incident. The shooter killed a teacher and a police officer. The man, a 9th-grader, may face a prison term of ten years.