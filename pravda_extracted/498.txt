La Liga: 9 games, 39 goals

La Liga gave another example of spectacular football with 39 goals coming from nine games. Atlético shot seven past Getafe, Real won five-nil away at Almeria and in San Sebastian, Real Sociedad and Celta gave another seven-goal thriller, 4-3 to the home side. La Liga is hot!
La Liga (After 14 matches)
It's Barça, Atlético and Real 1,2,3, Barcelona on 40 points after thumping Granada 4-0, Atlético three points behind (7-0 winner against Getafe) and Real Madrid third on 34 points, with a 5-0 away win at Almeria.
Premiership (After 12 matches)
In the Premiership one thing is certain: Arsenal FC (28 points) will enter December on top of the league, because after the 2-0 home defeat of Southampton (fifth on 22 points) and the Merseyside derby ending with a 3-3 draw at Everton, Liverpool (second) is 4 points behind. Chelsea remains in third place (24) with a 3-0 away win at West Ham United. Manchester City moves up to fourth with a 6-0 thrashing of André Villas-Boas' Tottenham Hotspur (in crisis), while Manchester United went down in Wales 2-0 at Cardiff City.
Bundesliga (After 13 matches)
Guardiola is doing a great job at Bayern (35), 3-0 winners away at third-placed Borussia Dortmund (28), virtually turning the Bundesliga into a two-horse race with second-placed Bayer 04 Leverkusen (31), 1-0 winner away at hertha in Berlin.
Serie A (After 13 matches)
Juventus leap-frogs over AS Roma after beating Livorno 2-0 away, while Roma was held at home by Cagliari, while Napoli lost more ground with a 1-0 home defeat by Parma. Juventus has 34 points, Roma 33 and SCC Napoli, 28.
Ligue 1 (After 14 matches)
Wins for the top three in France leave Paris Saint-Germain top with 34 points (3-0 at Reims), LOSC Lille second (30) with a home defeat over Toulouse (1-0) and third is Monaco (29) with an away win 1-0 at Nantes.
Primeira Liga (After 10 matches)
FC Porto (24) is now only one point ahead after losing two more points, this time at home to Nacional (Madeira), with a 1-1 draw. Second is Sporting CP on 23 with a 1-0 away win at Guimarães, while third and also on 23, is Belfica (1-0 at home over Braga).
Russian Premier League (After 17 matches)
The winter ice melts in Russia with the hottest season for a long time. Zenit (36, 2-0 home defeat by Rostov) leads over Lokomotiv (36, 1-0 winner over Dinamo Moskva) while Spartak and CSKA come third and fourth on 33 (CSKA won the derby over Spartak 1-0).
Fenerbahçe leads the Turkish league by six points (31) over Kasimpasa SK after all teams have played 12 games.
In Armenia FC Ararat leads, six points clear, with 27 points from 13 games; in Azerbaijan, Qarabag (29) and Nefti (28) points from 14 games set the pace; in Belarus after 31 games, BATE Borisov is the Champion and is seven points clear (64).
After 32 games in Kazakhstan, FC Aktobe has 43 points and is 1 point ahead of FC Astana.
In Ukraine after 17 games, the top spot belongs to Shakhtar Donetsk (38 points), three points ahead of FC Chornomorets Odessa. FCDnipro Dnipropetrovsk and FC Metalist Kharkiv have 34 points and 15 games played, two in hand.
Timothy Bancroft-Hinchey
Pravda.Ru