Russian woman dies from severe alcoholic poisoning in Egyptian resort

 
A Russian female tourist died of alcohol poisoning in Egypt's resort of Sharm el-Sheikh, officials with the consular department of the Russian embassy in Cairo, said, citing a police resort. The accident occurred on the night of January 1 - the woman's body was found in her hotel room.
According to police, the cause of death was established as the state of extreme alcoholic intoxication. It was said that such incidents occur three or four times a year.
Insurance companies do not cover such incidents, so relatives of the victims will have to pay all costs associated with the transportation of the body to Russia, said the consulate.
 