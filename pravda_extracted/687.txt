Winter Olympic Games in Sochi may look like summer ones

 
According to weather forecasts, Winter Olympic Games in Sochi may turn out to be hot. Meteorologists predict warm weather and rain in February.
On the opening day of the Olympics, February 7, sunny weather is expected with variable cloudiness and +7 ... +9 °C. February 15 and 16 will be the warmest days, when temperatures may rise to 18 degrees above zero Centigrade.
Russia's EMERCOM also said that the melting of snow in the mountains and rain may lead to floods and mudslides.
However, organizers of the Games said that weather would not disrupt the competitions. According to them, the snow depth at Sochi slopes and mountain objects exceeds meter-high marks.
President Vladimir Putin also told reporters that there was enough snow at sports facilities that would be preserved due to cold night temperatures.
 