In Sochi, Americans support Russian law against gay propaganda

 
Several Russian publications wrote that on February 8, 2014, a group of Americans gathered in front of the railway station in Sochi. The people were holding placards, on which they wrote words of support for President Putin in his fight with the propaganda of homosexual relations. The slogans were written in both Russian and English languages. It was said that the picket was organized by activists of US-based movement Street Preachers.  In particular, the placards that the people were holding said: "Homo sex is sin" and "God Bless Putin for he is against sin."
The action "Street Preachers" was not coordinated with local authorities, but law enforcement officials did not take any active steps to stop it.  According to the rules during the Winter Games in Sochi, all public meetings should be conducted only after they have been coordinated with local law-enforcement agencies and city authorities.
 
 
 