Mariposa virus creator jailed for four years

 
Slovenian hacker Matjaz Skorjanc, who created the malware that infected more than 12 million computers, was sentenced to four years and ten months in prison. The virus called "Mariposa" ("butterfly" in Spanish) was developed to steal credit card data and other personal information from personal computer.
The hacker, known for his nickname as Iserdo, was selling computer viruses before. In 2010, he was arrested, but then released on bail. The man continued his activities and was arrested in 2011.
Investigators believe that former medical student Matjaz Skorjanc earned around 500,000 euros from selling the virus. The damage that his malware caused is evaluated at tens of millions of euros.
 