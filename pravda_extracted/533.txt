Crimea asks $6 billion from Russia

 
The government of the Autonomous Republic of Crimea asks Russia to provide economic aid in the amount of six billion dollars. One billion would be direct financial support, whereas the remaining five would serve as investment, vice-speaker of the Federation Council Yevgeny Bushmin said.
The Ministry of Finance of the Russian Federation has drawn up a plan of assistance that would soon be sent to the government for approval in the coming days.
Two days ago, officials of the government of the Leningrad Region decided to transfer a part of their salaries to support residents of the Crimea. Regional governor Nikolai Drozdenko also instructed the Finance Committee to prepare proposals to assist the Crimea.