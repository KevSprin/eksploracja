Japan to catch space debris with fishing nets

 
The Japanese Space Agency placed an order with a maker of fishing nets for an extra-strong net to collect space garbage.
The project involves leading engineers and designers. They are to make a net about one kilometer long and 30 centimeters wide to clean near-earth orbit from 100 million small pieces of debris. The net will be made of three layers of interwoven metal fibers to make it super strong.
Artificially created magnetic field will be created to collect debris. Tests are to be held in February of the current year. The mission is scheduled for 2016.
 