Canadians cancel all press conferences in Sochi

The administration of the Canadian hockey team cancelled all official press conferences in Sochi, the Olympic News Service reports. At first it was reported that team Canada was going to cancel the press conference of head coach Mike Babcock, which was scheduled for today, February 12. The conference was supposed to take place prior to team training.
Some time later, the Canadians decided to cancel all official press conferences with the participation of the coaching staff. The reasons for the move have not been specified.
 