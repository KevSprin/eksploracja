Scenario of inequality

The scenario of socioeconomic inequality worldwide, reaffirmed in various reports issued by international organizations like the UN, FAO, ILO, World Bank and others, is most unfortunate: one in three people do not have access to electricity , one in five does not have access to drinking water, one in six is illiterate. One in seven adults and one child in three suffers from malnutrition.
Marcus Eduardo de Oliveira  Every five seconds a child dies of hunger in the world, one person in every seven suffers from chronic hunger. Nineteen children under five years old die every five minute victims of pneumonia; 500,000 mothers every year die in childbirth due to inadequate medical care; 5 million children every year do not complete five years of age, do not enjoy five years of life.
 Just over 300 million people worldwide have a life expectancy of less than 60 years, partly due to poor diets and on behalf of pathologies resulting from this lack.
 Thirty- five percent of the world population does not have enough energy and protein in the diet. Worldwide, there are two billion people who are anemic, including 5.5 million who inhabit the countries of advanced capitalism.
 As a result of chronic malnutrition, about 500 million children located in Latin America, Asia and Africa are at risk of permanent sequels in their bodies over the next 15 years.
 It is never too late to remember that we inhabit a world where the daily cost to feed a child with all the vitamins and nutrients needed only costs 25 US cents.
 According to NGOs (Save the Children), the death of 2 million children a year could be prevented if malnutrition were approached correctly.
 However, there exists pervasive exclusion, segregation, separation among peers, making inequality an incurable wound. 20 % of the world population - or one in five people - is excluded from participation in the consumption of food and other goods.
 According to the Human Development Report (RDH-2013), the United Nations Program for Development (UNDP), about 1.57 billion people (30 % of the population of 104 countries surveyed in the report cited) are in a situation of multidimensional poverty (it is said to possess multidimensional multiple definitions and ways of measuring it). Brazilians who suffer from multidimensional poverty are 2.7 % of the population. The United Nations Food and Agriculture Organization (FAO) states that 48 % of the 12.5 million children located in Latin America engaged in agriculture or family subsistence.
 The Report issued by the International Labour Organization (ILO) , attests that the number of poor people increased between 2010 and 2011 in 14 of the 26 developed economies analyzed, including the United States, France, Spain and Denmark.
 Also according to the ILO, there are more than 200 million unemployed persons around the world. The expectation is that by the end of 2015, this number will reach 208 million.
 From the data in the report "Credit Suisse Wealth Report 2013", 0.7 % of the world population concentrates 41 % of global wealth, while 50 % of adults worldwide have 1 % of the wealth. The global wealth reached this year a record U.S. $ 241 trillion.
 On one hand the exuberance of billionaires and on the other, the drama of the hungry. Just the United States focuses 37 of the top 100 billionaires in the world. In Russia there are another 11. In Germany there are six, while India and France has five, four respectively.
 The joint fortune of the three greatest billionaires of the planet (Bill Gates, Carlos Slim and Amancio Ortega) reaches at 200.3 billion dollars.
 While the wealth of the privileged is growing, the World Bank study elaborates by pointing out that to eliminate extreme poverty would require U.S. $169 billion per year (0.25 % of world GDP) or nine times less than is spent on military expenses in the world, whose frightening figure comes to $1.6 trillion.
 It appears that expenses on military equipment are the priority. And thus walks Humankind. What a sad world this is!
Marcus Eduardo de Oliveira is an economist, professor and specialist in International Politics.
prof.marcuseduardo@bol.com.br
  
  
 