Ukrainian border guards see invisible Russian armored vehicles

 
Representatives of the State Border Service of Ukraine stated that there were armored vehicles seen in the area of ​​the "Crimea-Caucasus" crossing.
In addition, Ukrainian officials reported that "specially-equipped troops of Russia" attacked Ukrainian border guards yesterday. They also said that the Russian troops supposedly used special equipment to suppress cellular signals.
Meanwhile, deputy director of Anroskrym company that is engaged in the organization of a railway ferry on the crossing, stated that there were no armored vehicles on the crossing.  There are just a few civilian vehicles there, the official added.
 