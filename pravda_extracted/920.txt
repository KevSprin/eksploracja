Number of videos containing child porn grows in Russia

 
According to authorities, the number of online videos containing pornographic materials depicting minors has increased 12 times in Russia over the period from 2009 to 2013.
Commenting on the situation, the Presidential Commissioner for Children's Rights, Pavel Astakhov, suggested that the frightening trend could be associated with the promotion of pornography in general and the normality of the emphasis put on issues of sexuality among minors.
Attempts to propagate non-traditional sexual relationships, including pedophilia, can only stimulate the process, the official said.
"Compared to the same period last year, the increase was 2.3 times," Astakhov said.
In addition, the number of sites that distribute "adult" pornography has increased by 20 percent. The situation remains extremely dangerous, despite the fact that a year ago, Russia effected the roster of illegal online resources. 
As is well-known, the situation on the Internet reflects the state of the society. Earlier it was reported that the number of sexual offenses committed against minors had increased dozens of times.