Any calls for separation of territories should be punishable, Putin says

  
Russian President Vladimir Putin believes that calls for separation of territories from Russia should not be left unanswered.
"One should not pass this by, not noticing such things, one should always give a response to statements, let alone actions," said the head of state at a meeting with law students, stressing that "the degree of protection should also be adequate."
According to Putin, calls for separation of Russian lands go against the Constitution of the Russian Federation.
"The Constitution explicitly says that the state shall ensure the territorial integrity of the country. Therefore, any remarks about separation of any territories of the Russian Federation are unconstitutional," he stressed.
"As for responsibility, one should not go too far, and one should not overstate the harm that such statements bring," said Vladimir Putin.
 