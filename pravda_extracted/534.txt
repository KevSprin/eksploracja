Aeroflot sacks drunk pilot, who steals 10 hours from 180 passengers

 
Aeroflot has fired the pilot, who was not allow to work on the flight from Blagoveshchensk to Moscow due to alcoholic intoxication, an official spokesman for the airline said.
The company also sued the pilot for damages. The amount of the lawsuit has not been specified.
The incident occurred on March 4. During preflight inspection, doctors noticed "improper" behavior of the pilot of the passenger aircraft. The pilot was sent to a drug abuse clinic, where medics found 0.85 ppm of alcohol in his blood.  Because of the drunk pilot, the flight, for which 180 passengers had been registered, was detained for ten hours. As a result, the flight was conducted by another pilot, who arrived from Moscow.
 