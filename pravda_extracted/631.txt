Remote-controlled bomb explodes in trolleybus in Volgograd, at least 15 killed

 
Another explosion rocked the city of Volgograd in the morning of December 30th. The bomb blew up in a trolleybus. The blast in the trolleybus killed 15. As many as 23 people were hospitalized, including 12-month-old child. 
The blast occurred on Kachintsev Street. The bomb in the trolleybus blew up as it was only 50 meters from a stop. The explosion seriously damaged the trolleybus and shattered many windows in neighboring buildings.
According to most recent information, it was not a suicide bomber, who exploded the trolleybus. The explosive device had been placed in the trolleybus and was then detonated remotely.
"The trolleybus was literally blown to pieces, only the front part of it was left more or less whole. The roof became like a dome from the blast wave," officials said.
The windows and walls of the trolleybus have been completely destroyed. The bomb was placed somewhere in the middle of the vehicle. Dead bodies were seen lying on the ground on the site of the explosion.
The explosion occurred during the morning rush hour on a busy city street lined with a market place on one side and a department store on the other side.
Eyewitnesses report that most people, who use public transport to get to work, disembark from buses, trolleybuses and trams and walk instead.
Also read: Suicide bomber explodes herself at train station in Volgograd