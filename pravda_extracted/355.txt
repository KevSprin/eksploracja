Man poisons 1,000 stray dogs in less than 1 year

 
The police of Vladivostok detained a local resident, who is suspected of the murder of at least a thousand of stray dogs. The man, who had no criminal record, poisoned about 1,000 stray dogs from May 2013 to January 2014.
When searching the apartment, the police seized the man's computer, two pistols, a rifle, four plastic bags filled with a brown substance and packages from medicines. The suspect said that he had learned about the composition of poison on the Internet and bought the necessary drugs in a drug store.
If convicted, the man can be arrested for six months.