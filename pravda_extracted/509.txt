Russian PM Medvedev receives dual-screen YotaPhone


Russian Prime Minister Dmitry Medvedev received a new gift - the first Russian smartphone made by Russian Technologies Corporation. The gadget with two screens is called YotaPhone.
Reportedly, the head of Russian Technologies, Sergei Chemezov, gave the phone to Medvedev saying: "I promised that you will be the first to own one, so here it is."
The Russian smartphone has two screens. The LCD one is on the front, while the black and white screen with "electronic ink" is on the back side.
It is said that the second screen can be used for reading eBooks, notifications or maps, as the screen consumes power only during updates.
The dual-screen smartphone has already received several awards, including Best of CES award and a prize of the International Advertising Festival "Cannes Lions."Today, Russian company YotaDevices announced the start of sales of the YotaPhone. The smartphone will go on sale in Russia and Europe priced at 19,990 rubles and 499 euros respectively.
 