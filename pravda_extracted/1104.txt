World oil prices grow against the background of Ukraine news

On March 3, 2014, world oil prices went up against the backdrop of news of instability in Ukraine.  The price of April futures on North Sea Brent Crude Oil rose by 1.46 % - to 110.66 dollars per barrel by 08.45 MSK vs. Friday's closing trade. The price of March futures for WTI light crude oil rose by 1.18 % to $103.80 per barrel.  On March 1, the Federation Council voted unanimously to give President Vladimir Putin consent for the use of the Russian armed forces in Ukraine to normalize the political situation in the country.
In return, the White House announced the G7countries (Canada, France, Germany, Italy, Japan, the UK and the U.S.) terminated their preparation for the summit of the Group of Eight in Sochi.
 