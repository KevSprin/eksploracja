Maidan sniper kills companion of former Georgian president

 
A Maidan sniper shot a companion of former Georgian President Mikhail Saakashvili. Previously, the Interior Ministry reported about victims among law enforcers and interior troops.
One of the members of United National Movement led by Mikhail Saakashvili, David Kipiani, was killed in Kiev today, Feb. 21. Eyewitnesses claim that he was shot by a sniper. Paramedics could not save the Georgian.  Saakashvili himself refused to confirm that Kipiani was staying in the Ukrainian capital for political reasons. The Georgian ambassador in Kiev has not commented on the situation.  This is not the first death of Georgian citizens on Maidan. Fifty-four-year-old Zurab Khurtsia had previously died of a heart attack. Khurtsia was not a Maidan  activist either.  In the morning of February 21, despite Rada's ceasefire decision, gunshots could be heard on Kiev's Maidan again. Several soldiers of interior troops and Berkut fighters were wounded, the Interior Ministry said.
Meanwhile, Yanukovych plans to sign an agreement with the opposition on February 21.
 