Unprecedented security measures introduced in Sochi one month before the Games

 
Sochi, the capital of Winter Olympic Games 2014, introduced unprecedented security measures on January 7th until the end of the Games. The Winter Olympics will be held on February 7-23. Paralympic Games will take place on March 7-16.
A month before the start of the Games, all units of the Russian EMERCOM go on duty to provide security to guests and participants of the Games.
"All sports facilities will be protected and monitored, with the use of space monitoring system. All security issues at Winter Olympics are being solved at the highest international level," officials said.
 President Vladimir Putin's decree that allows the authorities of Sochi to  regulate the activities that are not directly related to Olympic and Paralympic Games comes into force on January 7th. In accordance with the document, "all meetings, rallies, demonstrations, marches and pickets in the period from January 7 to March 21, 2014 shall be conducted in the places and (or) on the routes of the movement of participants of public events, with the number of participants and time intervals be determined by the municipal administration of Sochi in coordination with the respective regional office of the Interior Ministry and the respective regional office of security."
Thus, non-sports meetings and rallies are allowed in a specially equipped area.
 