Russia regrets EU's move to indefinitely delay visa regime talks

 
Russian diplomats expressed regrets at the intention of the EU to indefinitely postpone the dialogue on the visa-free regime with Russia.
Anvar Azimov, a Special Ambassador at the Russian Foreign Ministry, said after the European Commission presented the report on Russia's efforts on the implementation of joint steps that "the policy of the European Union to artificially contain Russia contradicted to cooperative arrangements and postponed the implementation of the strategy designated in 2003 to abolish the visa regime."Representatives of the parties intend to discuss the issue again on January 17 in Moscow at the meeting of the Permanent Partnership of the EU-Russia Council.
 