Eusebio leaves soccer world poorer

Eusébio, has died in Lisbon aged 71 of a cardiac and respiratory failure. However, the magnificent mix of power, vision and skill which characterized his football, and his dignified presence with the Benfica and Portugal soccer teams for decades after he stopped playing, will remain his legacy forever.
The Black Panther, the King, the Benfica and Portugal star known as Eusébio, has died in Lisbon aged 71 of a cardiac and respiratory failure. However, the magnificent mix of power, vision and skill which characterized his football, and his dignified presence with the Benfica and Portugal soccer teams for decades after he stopped playing, will remain his legacy forever.
Eusébio's biography and history on the field are well documented and today the epitaphs will be flowing in of fond memories of a nice, simple man who avoided the limelight and who would affably sign autographs in the favourite haunts he frequented in the area of Praça de Espanha, in Lisbon. The best epitaph, perhaps, is that Eusébio was a public figure for the vast majority of his life, both on and off the football field, in a highly and increasingly media-friendly game, and managed his public image very well, finishing his days as a highly respected,senior senator of football, of the national and club teams he graced with his unequalled presence, Portugal and Sport Lisboa e Benfica.
How many people who have been public figures for over half a century and who excelled in their professions, can claim the same?
For the record...
For the record, Eusébio's record speaks for itself:
Sporting Lourenço Marques: 1960, Provincial Champion of Mozambique, District Champion of Lourenço Marques
SL Benfica: 11 Portuguese Championships: 1960-61, 1962-63, 1963-64, 1964-65, 1966-67, 1967-68, 1968-69, 1970-71, 1971-72, 1972-73 , 1974-1975
5 Portuguese Cups: 1961-62, 1963-64, 1968-69, 1969-70, 1971-72
1 European Club Championship: 1961-1962
3 Ribeiro dos Reis Cups: 1963-64, 1965-66, 1970-71
9 Portuguese Super Cups: 1962-63, 1964-65, 1966-67, 1967-68, 1968-69, 1971-72, 1972-73, 1973-74, 1974-75
Toronto Metros-Croatia: 1 NASL title (1976)
Portugal: Third place in the 1966 World Cup, in England
 
Timothy Bancroft-Hinchey
Pravda.Ru
 