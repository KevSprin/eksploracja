Russian airlines will not be allowed to buy old aircraft


After the air crash in Kazan, Russian airlines may not be allowed to buy used foreign aircraft, first deputy chairman of the Duma Committee for Industry, Vice President of the Russian Engineering Union, Vladimir Gutenev said.
According to the official, there are no specific figures available yet; there is only an idea.It is also planned to develop a range of measures of financial support for airlines operating domestic aircraft. According to Gutenev, if an airline buys or takes on lease a new Russian aircraft, and returns it to the the lessor or sells it to third world countries in five years, the company will be allowed to lease the next new aircraft on preferential terms.Will it help solve the real problem? Why do they want to ban old aircraft, if it was a pilot error that caused the plane crash near Kazan? Share your opinion.
 