Sale of train tickets to Crimea suspended

 
Advance train ticket sales from Kiev to the Crimea have been temporarily discontinued.
"The movement of trains to the Crimea is carried out according to schedule, but due to technical reasons, advance ticket sales for the trains in the Crimean direction have been suspended," the message posted on a website of a Ukrainian railway company said.
At the same time, according to First Deputy Prime Minister Rustam Temirgaliev, one can travel from Russia to the Crimea through the Krasnodar region of Russia.
 