Bangladesh workers stop their strike after monthly salary raised to $68

 
About 250 garment factories in Bangladesh reopened after a meeting with the Prime Minister of the country, Sheikh Hasina. Factory workers went on strike demanding a raise of minimum salary up to $100 a month, while the previous figure was $40.
As a result of negotiations and government recommendations, factory owners agreed to raise people's wages by 77 percent - to $68 a month. Workers will start to receive new wages from December 1 2013, Labor Minister of Bangladesh Rajiuddin Ahmed Raju said.
 
 