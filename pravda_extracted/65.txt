Russians owe banks 185 billion rubles

 
The volume of overdue bank loans as of the end of 2013 will amount to 185 billion rubles in Russia vs. 160 billion rubles in 2012.
The study conducted by Sequoia Credit Consolidation collection agency says that in 2012, the amount of arrears in the work of collectors increased by 20.5 billion rubles.
Currently, 78 percent of bank arrears account for consumer loans (including credit cards and cash loans), 14 percent - on auto loans, 3 percent - on mortgage. In 2012, consumer loans accounted for 75 percent and car loans - 8-10 percent.  At the same time, those, who already have unpaid debts, continue to raise more loans to pay their previous debts.
Analysts say that in 2011, an average borrower had one or a maximum two loans. Today, one borrower holds 3-5 existing loans. This leads to the fact that the share of monthly income of a borrower that he or she gives to repay their debt reaches 45 percent, which significantly increases the risk for such a customer to hold overdue payments.
Central Bank specialists say that the amount of population's arrears on loans currently stands at 435 billion rubles. In other words, credit institutions this year sold almost a half of delay to professional collectors. 
 