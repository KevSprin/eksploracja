Moscow region mourns victims of apartment building explosion

 
The Moscow region announced November 14, 2013 the day of mourning for those killed in a gas explosion in an apartment building. 
Flags will be flown at half mast tomorrow on the entire territory of the Moscow region. Authorities recommended organizations, television and radio companies to refrain from broadcasting entertainment programs.
The tragedy took place in the town of Zagorskiye Dali, in the Sergiev Posad district. An explosion in the apartment building killed seven people. 
The building is to be restored before 1 March 2014. It was reported that the authorities of the Moscow region would cover all costs to bury the victims.
 