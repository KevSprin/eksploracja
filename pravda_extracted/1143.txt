Up to 300 Russian tourists lost in the Philippines

 
From 100 to 300 Russian tourists found themselves trapped in Manila and other regions of the Philippines due to the typhoon. There is no communication with the Russian tourists; their exact location has not been established yet, Third Secretary of the Embassy of the Russian Federation to the Philippines, Alexei Ilyuviev said.
The Russian Union of Travel Industry has not received any information on casualties among Russian tourists yet.
The Russian Embassy in the Philippines is doing everything possible to establish connection with the local authorities, the Emergencies Ministry, to find out any information about the whereabouts of the Russians. An aircraft of the Emergency Situations Ministry is ready to fly to Manila for the evacuation of the Russian tourists.
The typhoon struck the islands of the central Philippines on November 8. Official and unofficial data on the number of those killed in the disaster vary widely. According to official data, the number of victims exceeds 1,500 people, according to unofficial data - the typhoon killed over 10,000.
 