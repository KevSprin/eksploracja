Criminal case against Yulia Tymoshenko closed due to lack of evidence

 
The Kievsky District Court of the city of Kharkov dismissed the criminal case against Yulia Tymoshenko about the debt to the Defense Ministry of the Russian Federation on the part of  United Energy Systems of Ukraine, the press service of Fatherland party said.   The case has been closed due to lack of evidence. In addition, the arrest of Tymoshenko's property has been lifted.
On February 22, 2014, Yulia Tymoshenko left the colony in the Kharkov region, after the Verkhovna Rada voted for her release. Tymoshenko was serving there her seven-year sentence for abuse of power when signing gas contracts with Russia in 2009. On January 18, 2013, new charges were brought against her on the case of the debt to the Defense Ministry of the Russian Federation on the part of United Energy Systems of Ukraine. To crown it all, Tymoshenko was charged with plotting the assassination of deputy Yevgeny Scherban in 1996.
 