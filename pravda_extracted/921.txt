Barça, Real, Atlético with horns locked

Barcelona, Real Madrid and Atlético Madrid are locked on 57 points apiece, separated by goal difference. In the Premiership, José Mourinho's Chelsea FC sits atop the league after Liverpool managed to stay in touch with the pack by thrashing Arsenal. In Portugal, the Lisbon derby between Benfica and Sporting was called off after the covering of a stand at the Estádio da Luz disintegrated in a storm.
La Liga (After 23 matches) 
Atlético's reign at the top of La Liga did not last long, the Madrid team being replaced by Barcelona and overtaken by Real Madrid after losing 2-0 at Almeria Barcelona (4-1 at Sevilla) leads with 57 points and 63 goals scored, 17 against (GD 46), Real Madrid is second (57 points, 65-24, +41) after winning 4-2 over Villarreal. Atlético has a goal difference of +40 (56-16).
Premiership (After 25 matches)
Arsenal (second place, 55 points) is off the top of the Premiership after Arsene Wenger's pupils were thrashed 5-1 at Liverpool (50). Chelsea is first on 56 after beating Newcastle United 3-0, Manchester City is third on 54 points after a goalless draw at Norwich City.
Bundesliga (After 20 matches)
Bayern Munich is the runaway leader on 56 points, thirteen clear of Bayer 04 Leverkusen (43), with Dortmund on 39. All the top three won their matches. Bayern won 2-0 at Nurnberg, Leverkusen won 1-0 at Moenchengladbach, Dortmund won 5-1 away at Bremen.
Serie A (After 23 matches) 
Juventus leads on 60 points, AS Roma is second with 51 and a game in hand and Napoli is third with 47 points. Juve drew 2-2 at Verona, while Napoli beat AC Milan 3-1.
Ligue 1 (After 24 matches)
PSG leads with 55 points 5 clear of second-placed AS Monaco (where both teams drew 1-1). FC Lille is third with 44 (2-0 against Sochaux).
Primeira Liga (After 18 matches)
FC Porto took advantage of the no-result between Lisbon duo Benfica and Sporting (problems with the stadium cover during a storm) by winning 3-0 against Paços de Ferreira. Benfica leads on 40 points, FC Porto has 39 and Sporting, 38. Benfica and Sporting have that game in hand.
Turkish League (After 20 matches)
Fenerbahçe (44) leads the Turkish league but with a gap reduced now to just four points over Galatasaray AS (40). Besiktas JK is third with 36.
In Azerbaijan, the Championship saw another change at the top after 20 games. Qarabag is back on top with 39 points, Neftci has 38 (second) and Qabala is third with 30 also.
Russian Premier League (After 19 matches)
The Russian Premier is hot, hot, hot. Zenit recovered with a 2-1 win over Ural and is equal first (40 points) with Lokomotiv (drawless home game with Rubin). Spartak Moskva is third with 39 points (1-0 winner at Rostov).  Dinamo Moskva (35) beat Amkar 2-0 in Moscow, while CSKA (34) lost 1-0 at Krasnodar. The championship resumes in March.
In Armenia FC Ararat leads, 8 points clear, with 30 points from 14 games. The Championship resumes in March; in Belarus after 32 games, BATE Borisov is the Champion. In Kazakhstan, FC Aktobe is champion.
In Ukraine after 18 games, the top spot belongs to Shakhtar Donetsk (41 points), five points ahead of Dynamo Kyiv. Metalist Kharkiv (36) and Dnipro Dnipropetrovsk (35) follow in third and fourth place, both with a game in hand. The championship resumes in March.
Timothy Bancroft-Hinchey
Pravda.Ru