Boeing-737 crashes when landing in Kazan, leaving no survivors

 
Boeing-737 passenger plane crashed when landing at the airport of Kazan. The plane of Tatarstan airline was flying from Moscow's Domodedovo Airport, according to the Ministry for Emergency Situations of the republic of Tatarstan. The crash left no survivors.
According to the Federal Air Transport Agency (Rosaviation), there were 50 people on board: 44 passengers and six crew members.
The message about the crash came at 19:25 Moscow time. The aircraft was due to land at 19:40 MSK at the airport of Kazan. 
The head of the department for propaganda and public relations of the Ministry for Emergency Situations of Tatarstan, Andrey Rodygin, told Interfax that the plane "exploded" during landing. At the same time, it was also reported that the plane caught the ground when flying for a second round after a failed attempt to land.
Life News reports that Boeing's fuel tank exploded when the plane touched the ground.
The airport is closed before 23:00 MSK.
TV channel Russia-24 reported that President Putin ordered to urgently form a commission to investigate the causes of the tragedy.
Two versions are being analyzed: a pilot error and technical malfunction. Weather conditions were not ideal: wind of 8 m/s with gusts up to 12, visibility to 5 miles, moderate snow and rain.
According to visitors of forumavia.ru, the number of the crashed plane was VQ-BBN. The age of the aircraft was 23.4 years; its maiden flight was made on ​​June 18, 1990. 