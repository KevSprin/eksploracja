Domodedovo airport bombers sentenced to life in prison

 
The Moscow Regional Court sentenced four suspects on the case of the Moscow Domodedovo Airport bombings to life in prison.
According to the ruling, Islam and Ilez Yandiyev and Bashir Khamkhoev were sentenced to life imprisonment in a special regime colony. Another accomplice of the terrorists, Ahmed Evloev, was sentenced to ten years in a penal colony. In addition, the convicts must pay from 50 to 500 thousand rubles to victims.
Investigation determined that the organizer of the attack was the leader of Imarat Kavkaz terrorist group, Doku Umarov.
During the trial, Islam Yandiev regretted his participation in the crime. Bashir Khamkhoev asked to be acquitted for lack of evidence. The brother of the alleged suicide bomber, Ahmed Evloev, claimed he was unaware of the impending terrorist attack.
The explosion occurred in the arrival hall of international airlines of Domodedovo Airport on January 24, 2011. The blast killed 37 Russian and foreign citizens, 172 people were injured.
 