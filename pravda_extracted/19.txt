Putin: There was anti-constitutional coup in Ukraine

Vladimir Putin said at a meeting with reporters in his residence of Novo-Ogarevo in the Moscow region that there can be only one assessment given to the events in Ukraine.  This is an anti-constitutional coup and seizure of power.
"The question is why it was done. President Yanukovych signed the agreement with the opposition and surrendered his power. He agreed to hold the early elections of the president and the parliament and ordered to take police forces out of the capital."
"As soon as he went to take part in an event in Kiev, his residence was taken along with the government building. What was it done for? He surrendered his power and had no chances to be reelected. What was the need to drag the country into the chaos, in which it finds itself now? These were absolutely silly actions. They achieved the result opposite to what they expected," Putin said.
 