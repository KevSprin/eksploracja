Russia and Kazakhstan establish Unified Missile Defense System

 
Russian President Vladimir Putin signed the law "On the ratification of the Agreement between the Russian Federation and the Republic of Kazakhstan to establish joint regional air defense system of the Russian Federation and the Republic of Kazakhstan."
The document stipulates that the establishment and the use of the unified air defense system shall be implemented on the basis of coordination of joint actions of troops (forces) of the unified air defense system in peacetime. The troops shall be subordinated to national commands and used in accordance with joint concepts and plans in wartime.
The troops of the Unified Air Defense System are led by the commander appointed by the presidents of the two countries following recommendations from defense ministries of Russia and Kazakhstan.
The command of the unified air defense system is located in Alma-Ata, Kazakhstan. The agreement was concluded for five years and can be automatically renewed for subsequent five-year periods.
 
 