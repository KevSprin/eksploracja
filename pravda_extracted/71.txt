ATMs in Sochi to be equipped with Chinese and French interface

 
Sberbank will equip ATMs in Sochi with Chinese and French interface for basic operations, Deputy Chairman of Sberbank Alexander Torbakhov said.
The introduction of the French language is one of the requirements of the IOC, and the decision to create Chinese interface was made for a large number of French-speaking tourists and delegates, who intend to come to Sochi.
Torbakhov also said that Sberbank was going to further extend these functions on the entire ATM network.
 
 