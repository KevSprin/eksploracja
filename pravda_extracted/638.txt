In Norway, TV presenter sacked for wearing Christian cross on air

 
Popular Norwegian TV presenter Siv Kristin Sællmann was suspended from her job for appearing on air with a cross on her chest. The woman's appearance on the screen with a gold pendant, decorated with black diamonds in the shape of a Christian symbol, caused outrage among a part of viewers.
Members of the local Muslim community organized protests claiming that "a chain with a cross insulted Islam." According to them, displaying the Christian symbol did not guarantee the impartiality of the TV channel.
As a result, the director of NRK television channel was forced to hold a conversation with the newsreader, after which she was suspended from her duties not to become a source of discord and crimes.
 