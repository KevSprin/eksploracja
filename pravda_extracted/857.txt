Russian spiritual and cultural center to appear in Paris

 
Prefect of Paris and Ile -de-France region, Jean Daubigny allowed the construction of a Russian Orthodox spiritual and cultural center in Paris.
Reportedly, the construction of the complex, which will consist of a church, a  seminary, a library and a center for meetings of the Russian community, will begin in June 2014, immediately after existing buildings are demolished.
The administration of the President of Russia purchased 4.2 thousand square meters of land in Paris​​ for the construction of an Orthodox cathedral back in 2010.
 