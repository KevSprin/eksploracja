Exchanging Bitcoin in Russia to be regarded criminal action

The administration of the Central Bank of Russia intends to oblige all banks in the country to report all transactions associated with the exchange of cryptocurrencies.
The production and exchange of virtual currencies can be attributed to activities related to money laundering and financing terrorism. Experts note that the intention of the Central Bank will agitate Bitcoin owners, but will not solve the problem of legal vacuum that has been formed around cryptocurrencies.
The Bank of Russia intends to equate the provision of services by Russian legal entities for the exchange of virtual currencies, including Bitcoin, to illegal activities related to money laundering and terrorist financing.
"The Bank of Russia warns citizens and legal entities, primarily credit institutions and non-credit financial institutions, against the use of "virtual currencies" for their exchange for goods (works, services) or cash in rubles and foreign currency," the message from the Central Bank administration said.
 