Mass brawl takes place near Crimean Parliament in Ukraine

In Ukraine's Simferopol, a mass brawl took place near the Crimean Parliament between those supporting the new government of Ukraine and the people protesting against it.
The Crimean Tatars took the side of the supporters of Euromaidan. Shouting "Allahu Akbar!" they broke through the cordon separating them from the pro-Russian groups. One of the participants of the scuffle suffered a severe head injury.
The parliament of the Crimea gathered for a special meeting on February 26 to develop a strategy of actions in light of the current state of affairs n Ukraine.
 