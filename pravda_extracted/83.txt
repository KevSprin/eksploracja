Russia's Saratov region bans noise and loud music

 
Saratov Regional Duma deputies made changes to the regional law on administrative offenses, prohibiting citizens to make noise from 9:00 p.m. to 9:00 a.m., the press service of the regional parliament said. Prior to the change, quiet time was limited to eight hours (from 11:00 p.m. to 7:00 a.m.).  The report said that the normative legal act established administrative liability for disturbing citizens's peace and quiet time, including by turning on loud music in the daytime.
In addition, deputies decided to increase penalties for violation of this rule. Violators of the law will have to pay the fine from one to two thousand rubles, officials - from two to five thousand rubles, legal entities - from 15 to 30 thousand rubles.