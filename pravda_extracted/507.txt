Australian astronomers find oldest star in the Universe

 
Astronomers of the Australian National University discovered a new object in the sky. According to them, the object is the oldest star in the universe. The age of the star is about 13.6 billion years. It is located relatively close to Earth in astronomical terms - about 6 thousand light-years.  The head of the group of astronomers Stefan Keller said in an interview with ABC that "the first generation of stars that were formed after the Big Bang were of gigantic size, but were highly volatile and lived for quite a short period of time.
 He also explained that they found a second-generation star. The scientists were very surprised when they made ​​this discovery, as the probability to find such a star is one in 60 million. Discovering older stars is impossible, says the astronomer.
The new star does not yet have a name and was registered as SMSS J031300.36-670839.3.