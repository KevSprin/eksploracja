Colombia: Towards peace

For the first time since 1964, when the Colombian Civil War began between the Government forces and FARC-EP (Armed Revolutionary Forces of Colombia-People's Army), there is a real chance of peace with a negotiated agreement for the political participation of FARC in Colombian life. Here are the main points of the agreement so far.
The agreement between FARC-EP and Colombian president Juan Manuel Santos is described as partial however for the future of Colombia it is fundamental, putting an end to almost half a century of conflict.
1. As regards participation in political life, the agreement covers the formation of transitory special peace constituencies to carry out public awareness campaigns and promote political activity in the areas most affected and these constituencies will be represented in the House of Representatives in Congress.
2. A security system is to protect the physical safety of politicians and their right to freedom of expression, particularly for the new movement arising out of the demobilization of FARC is to be set up - and here we must remember that the civil war was not only carried out by FARC and the Government forces but also Fascist paramilitary groups.
3. Implementation of measures to create the climate for the co0nstruction of political parties, providing special assistance to new movements after the peace treaty has been signed.
4. Implementation of a Statute of Opposition which creates space for the practice of political opposition and a fundamental revision of the entire Colombian political and electoral system, providing transparency in electoral processes.
5. Promotion of a new vector of citizenship in politics, providing guarantees to the more vulnerable members of society, while guaranteeing the right to political mobilization and citizen protest groups. Here we must remember the high level of political assassinations (murder) carried out against those defending Unions or left-wing policies in Colombia by Fascist forces close to the Government.
6. Regarding agrarian reform, one of the main areas of policy defended by FARC since its foundation in 1964 by Manuel Marulanda Vélez, the agreement is to set up an integral programme of agrarian and rural reform, in which the State invests in areas devastated by conflict.
7. The State is to provide technical assistance, subsidies, credit and options for formal work for the peasant population, providing them also with healthcare, education, housing and poverty eradication programmes.
8. Implementation of livestock and agricultural projects, creating a solidary and cooperative economic climate.
9. Return of lands to those whose properties were seized or who were forced to become displaced.
10. Creation of a fund to distribute lands among the landless peasantry.
11. Guarantee of food safety through a local production scheme.
Under discussion at present are the themes of drug production and sales, abandonment of weapons, indemnity to be paid to victims and the format of the final agreement to end the conflict.
From the Portuguese version of Pravda.Ru
Translation by Juan Blanco
Exclusive Interview between Pravda.Ru Portuguese version and Commander Raul Reyes in 2005 (in Portuguese).
http://port.pravda.ru/news/mundo/09-03-2005/7373-0/
 