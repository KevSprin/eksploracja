Illegal abortions in Russia to entail million-ruble fines

 
The State Duma of the Russian Federation gave the first reading to the bill that introduces fines up to one million rubles for illegal abortions.
It goes about violations committed by medical specialists during legally established procedures to perform abortions. The actions of a doctor shall not fall under the Penal Code, the bill specifies. The bill was initiated by MPs Alexander Zhukov, Elena Mizulina and Olga Batalina.
The draft law stipulates sanctions from 3 to 5 thousand rubles for citizens, from 10 to 50 thousand rubles for officials and from 500 thousand to one million rubles for legal entities.
According to Mizulina, despite the current trend for the number of abortions to reduce, the number of actual abortions performed in Russia was much larger, as the statistics takes account of the surgeries performed in municipal healthcare institutions.
Official numbers speak of 900,000 abortions a year, whereas experts say that the actual number of abortions is five-eight times larger. "It goes about millions of abortions," Mizulina said.
Deputies believe that the absence of legal responsibility for medical employees for violating legal norms leads to lawlessness and does not protect the right of pregnant women for motherhood.
Meanwhile, Russia's current law stipulates criminal responsibility only for illegal abortions in cases when a surgery is performed by a person who does not have adequate medical education.
 
 