Russian troops take Sevastopol Airport in Ukraine's Crimea under control

 
A group of armed people has left the airport of Simferopol in the Crimea, officials said. On February 28 overnight, as many as 50 men in unmarked uniforms broke into the building of the airport. The men arrived on three KamAZ trucks with no license plates.
The airport currently works according to its schedule. All arrivals and departures are being conducted normally.
Meanwhile, Russian military men have taken control of the airport in Sevastopol, Interfax reports with reference to military sources in the city.
Russian military men arrived at the airport of Bilbek near Sevastopol "to prevent the arrival of some gunmen," as it was said. Unidentified armed men seized the airport of Sevastopol on Feb. 28 overnight too.
Information about incidents at Crimean airports is still contradictory. According to Interfax, the goal of the military men at Bilbek Airport is not to let Ukraine's new Interior Minister Arsen Avakov and Security Service Chief Valentin Nalivaichenko arrive in the Crimea.
 