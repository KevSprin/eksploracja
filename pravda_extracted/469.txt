Launch of Soyuz booster rocket pushed back, reason not named

 
The launch of Soyuz-2.1B rocket, which was scheduled for December 25, has been postponed till 2014. Dmitry Zenin, a spokesman for Aerospace Defense Forces, said that a meeting of the State Commission would be held in the near future to set the date for the new launch.
Previously, the launch, initially scheduled for December 23rd, was postponed due to the need of additional refinement of the ground-based equipment. The reason has not been specified.
Light class booster rocket Soyuz-2.1B  was developed by Samara-based  company Progress. The two-stage rocket can carry space satellites weighing up to 2,800 kg to low circular orbit. 
 