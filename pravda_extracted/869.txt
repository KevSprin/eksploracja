India and Pakistan's nuclear war games to starve mankind to death

 
A study published in The Telegraph newspaper Tuesday said that even a limited exchange of nuclear missiles between India and Pakistan would lead to a fatal catastrophe on the whole planet.
Experts from the International Physicians for the Prevention of Nuclear War said that an exchange of nuclear blows between these two countries will lead to global hunger that will end the existence of human civilization.
According to experts, even if attacks are limited, the impact on the atmosphere will be so devastating that the entire harvest of the world will be destroyed. This will lead to chaos on the world food market.
Noteworthy, the group of scientists that was once awarded the Nobel Peace Prize, published the first edition of this document in April last year. In the previously published report, they paid the world's attention to billion of human  victims that could occur in the event Pakistan and India exchanged nuclear strikes.
In the second edition, the scientists expanded the theory by saying that they "underestimated China." According to their forecasts, one of the most populous countries in the world would face significant challenges in the food supply issues. A billion fatalities in developing countries is definitely a disaster, but if  one adds another 1.3 billion Chinese people from the group of risk, then mankind may come to what they call the end of civilization, the scientists said.
 