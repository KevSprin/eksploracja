World 's 21 million victims of forced labor

GENEVA / SWITZERLAND - In the world, about 21 million people are victims of forced labor. A Study by the International Labour Organization (ILO) notes that 20.9 million people are trapped in jobs imposed by means of coercion or deception and from which they cannot leave. Most are women, representing 55 % of the total, with 11.4 million estimated victims.
Victims of forced sexual exploitation make up 22 % of people subjected to forced labor. The work related to sex has "strong ties" with displacements across borders, the study found. Estimates are that 9.1 million people (44 %) who are victims of forced labor have moved within their country or abroad .  The main sectors in which there is exploitation of this type of labor are agriculture, construction, domestic work and industries, among which there is a 68 % share of victims .  Besides these sectors, linked to the private sector, 2.2 million (10 %) of the victims are victims of forced labor imposed by the state, such as in prisons - violating ILO standards - or imposed by armed rebel forces or national armies.  The concentration of victims of forced labor is higher in central and southeastern Europe, where countries have the mark of 4.2 victims of forced labor for every thousand inhabitants. The lowest concentration is in the Euro zone, with a ratio of 1.5 victims of this type of work for every thousand inhabitants. Latin America and the Caribbean have the rate of 3.1 per thousand inhabitants.  The average age of workers is also a concern raised by the study. Of the 20.9 million people, 5.5 million are under 18. 
By ANTONIO CARLOS LACERDA PRAVDA.RU
 ANTONIO CARLOS LACERDA is International Correspondent PRAVDA.RU
Translated from the Portuguese version
of Pravda.Ru
By Olga Santos
 
 
 