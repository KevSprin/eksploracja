Georgia's ex-President Mikhail Saakashvili banned from entering Ukraine

 
A Ukrainian deputy from the Party of Regions, Oleg Tsarev, said that the Security Service of Ukraine and the Ministry for Foreign Affairs upheld his request to ban 36 people from entering the territory of Ukraine. The "black list" includes former Georgian President Mikhail Saakashvili. 
In mid-December, former Russian Deputy Prime Minister Boris Nemtsov said that he was going to sue the Ukrainian authorities, who banned him from entering the country.
On December 7th, ex-Georgian President Mikhail Saakashvili appeared on  Independence Square in Kiev. He said during his speech that Russia was conducting a hostile takeover of Ukraine.
 