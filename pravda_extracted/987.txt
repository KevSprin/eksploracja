Russian Railways to bid farewell to anachronistic carriages

 
Russian Railways (RZD) President Vladimir Yakunin called third-class sleepers an "anachronism" and vowed to eventually replace them with more comfortable carriages. 
"I believe that such anachronism, as third-class sleepers must be replaced with more comfortable types of passenger carriages," said Yakunin.
According to him, this question depends on the reduction of costs and the procurement of new, more comfortable carriages. "I can not say whether it is a question of ten years, but I can say that it is not a matter of two years," said the head of Russian Railways.
 