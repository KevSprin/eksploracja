Real breaks free

Real Madrid took full advantage of the slip-ups of its arch rivals this season, leading the Liga by 3 points. Chelsea remains top in the Premiership with a goal from John Terry three minutes into extra time. In the Bundesliga, Bayern is in a league of its own.
La Liga (After 25 matches) 
Real Madrid is isolated at the top of La Liga, taking advantage of losses experienced by their rivals this year. Real beat Elche 3-0 and leads with 63 points. Barcelona (3-1 losers at Real Sociedad) is second with 60, equal on points with Atlético Madrid (3-0 at Osasuna's ground in Pamplona).
Premiership (After 27 matches)
Chelsea (60), Arsenal (59), Manchester City (58, one game in hand), Liverpool (56), the top of the Premier League. John Terry's goal against Everton after 90 + 3 minutes kept Chelsea on top. Arsenal trounced Sunderland 4-1 in London, Manchester City beat Stoke City at home 1-0 and Liverpool beat Swansea City 4-3.
Bundesliga (After 22 matches)
Bayern Munich is nineteen points clear at the top with 62 points. Second is Bayer 04 Leverkusen (43), followed by Borussia Dortmund (42), FC Schalke 04 (41) and VfLWolfsburg (39). Bayeern came away from Hannover 4-0 winners, Leverkusen lost 3-1 at Wolfsburg, while Dortmund went down 3-0 at Hamburg. Schalke was held 0-0 at home by Mainz.
Serie A (After 25 matches) 
Juve leads on 66 points after beating Torino 1-0. Second is AS Roma (57 points, 1 game in hand) after winning by the same result at Bologna, third is Napoli, after a 1-1 at home with Genoa, on 51 points.
Ligue 1 (After 26 matches)
PSG leads with 61 points 5 clear of second-placed AS Monaco. LOSC Lille is third with 46 , one ahead of AS Saint-Étienne. Paris Saint-Germain won 4-2 at Toulouse, aided by a hat-trick from Ibrahimovic; Monaco beat Reims 3-2; Lille and Lyon drew nil apiece and Saint-Étienne won 2-0 at Bastia.
Primeira Liga (After 20 matches)
FC Porto is passing through difficult times, losing at home for the first time in the Portuguese League in five and a half years, to Estoril (0-1), a result which complicates any repetition of the title this year. FC Porto is third on 42 points. Sporting CP beat Rio Ave 2-1 away and is second on 44 points. Benfica beat Vitória de Guimarães 1-0 in Lisbon and is top with 49 points.
Turkish League (After 22 matches)
Fenerbahçe (47, 1 game in hand) leads the Turkish league. Galatasaray AS is second with 44, two ahead of Besiktas JK.
 
In Azerbaijan, after 23 games the Championship is led by Qarabag with 46 points, Qabala is second with 42 points and Baki FC third on 40.
Russian Premier League (After 19 matches)
The Russian Premier is hot, hot, hot. Zenit recovered with a 2-1 win over Ural and is equal first (40 points) with Lokomotiv (drawless home game with Rubin). Spartak Moskva is third with 39 points (1-0 winner at Rostov).  Dinamo Moskva (35) beat Amkar 2-0 in Moscow, while CSKA (34) lost 1-0 at Krasnodar. The championship resumes in March.
In Armenia FC Ararat leads, 8 points clear, with 30 points from 14 games. The Championship resumes in March; in Belarus after 32 games, BATE Borisov is the Champion. In Kazakhstan, FC Aktobe is champion.
In Ukraine after 18 games, the top spot belongs to Shakhtar Donetsk (41 points), five points ahead of Dynamo Kyiv. Metalist Kharkiv (36) and Dnipro Dnipropetrovsk (35) follow in third and fourth place, both with a game in hand. The championship resumes in March.
Timothy Bancroft-Hinchey
Pravda.Ru
 
 
 
 
 
 
 
 
 
 
 
 
 
 