Ten things you hate about social networking

Nearly all Internet users are registered in at least one social network. People join them to communicate and do networking, and sometimes the reality of social networks makes them feel disappointed and annoyed. The trigger of the negative emotions is their "friends'" behavior. Let's look at ten most common mistakes made by social networks users.
Requests for "likes" and reports
"Vote for me in this contest," "Like" this clothing chain,"  "Repost this missing child report!" A user reading such posts faces an awkward choice between meeting the request of a "friend" and "logging" their timeline, or ignoring it and upsetting the author of the post. Meanwhile, the posted competition looks silly, you are not willing to advertise clothing chain, and spreading the information about the missing child somewhere in the Siberian taiga is unlikely to help find this child. In addition, "likes" and "reposts" takes time. If a "friend" post such messages daily it becomes really time consuming.
Clever posts
Many like posing "clever" statuses, including quotations of philosophers or witty remarks. For example: "Everything can be fixed", "Just so you know, I'm an optimist" or "Life is a chess board." Maybe these posts really reflect the user's mood, feelings and thoughts at the moment, but why share them? Not everyone is in a philosophical mood.
Tagging friends in photos It is fine if the picture is good, but what if it is not? If a user wants to tag someone, it is better to ask permission of the person first. Otherwise, the tagged "friend" may be offended.
Check-ins
Check-ins identify the user's current location. If the user tells their friends that they are currently in Paris or Hawaii, then a check-in seems appropriate. Some love checking in everywhere: "I'm in front of my house," "I'm in the supermarket," "I'm in a hospital." Informing "friends" of all movements is rather pointless.
Selfies
This is when people take pictures of themselves. Lately these pictures flooded social networks. There is nothing wrong with that if selfies are taken on the background of attractions. Another thing is taking them at home while sitting on a couch. Who cares? Plus, these images are often not that good looking.
Elevator selfies
Many like to be photographed in an elevator or in a bathroom while looking in the mirror. Taking a fun and stylish picture this way is not easy. It looks like narcissism, and people who are obsessed with their own persona are not generally liked.
Photos of weather forecasts at resorts
Oddly enough, many people like to post these pictures, likely to evoke jealousy in their "friends" at home who have to brave the cold. But jealousy is not a constructive feeling and does not contribute to sympathy.
Food photography
It is one thing when someone posts a recipe that they want to share with "friends," or a rare culinary masterpiece unknown to the general audience, or an interesting story associated with food. Sometimes people post pictures of a common salad or pizza with the caption: "Look what I made. This is beautiful." Few people are interested in what you ate for lunch at a cafe, unless it is some exquisite dish.  
Emoticons and stickers
There is nothing wrong with emoticons, but some are clearly overdoing it. Comments with an abundance of smileys, brackets and colons look illegible. Everything is good in moderation. At some point they start being distracting and annoying.
illiteracy
Clearly no one grades the user's grammar in social networks, but it is always a good idea to check a post before publishing. Network "jargon" is out of date, and not everyone likes reading texts riddled with mistakes.
Users should remember that their pages can be viewed not only by  "friends" but also  potential business partners and employers. If they find something "offensive" it may cause problems and missed opportunities.