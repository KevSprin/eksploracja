Vladimir Putin and the Pope hold long meeting in The Vatican Library

 
Within the scope of Vladimir Putin's visit to Italy, several important meetings and negotiations were conducted. 
In Trieste, which is considered the Italian gateway to Eastern Europe, intergovernmental consultations and a business forum will be held.
Straight from the airport, Putin went for a meeting with the Pope. The conversation took place in the Vatican Library behind closed doors. The meeting took a lot of time. Even Barack Obama spent just 20 minutes there. The discussed topics were not disclosed. It was only said that the Russian president and the Pontiff established a relationship of confidence due to the proximity of their positions on the conflict in Syria.
The Pope openly supported the position of Russia directed against military intervention in the country, despite the contrary opinion of the official Rome.
In conclusion of the meeting, Putin and the head of the Catholic Church exchanged gifts. The President gave the Pontiff an icon of the Vladimir Mother of God. The Pope presented all members of the Russian delegation with majolica depicting the Vatican gardens. 
Later, the Russian president met with the head of Italy, Giorgio Napolitano, with whom he also discussed, without witnesses, the Syrian problem.
Russia and Italy are to sign a number of important agreements during Putin's visit, including the one on the introduction of free visas for children up to 16 years.
 
 