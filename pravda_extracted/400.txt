Ukraine strengthens control over railway stations and highways

 
The Ukrainian Cabinet of Ministers instructed the State Border Service to strengthen control over the movement of persons on a number of railway stations and highways.  In particular, measures will be taken to control the movement of objects on the railways (Kherson, Vadim, Novoalekseevka, Melitopol) and highways (Chaplinka, Kalanchak, Salkovo).
In addition, the Ministry of Finance, the Ministry of Infrastructure, the administration of the State Border Service and relevant agencies of the Interior Ministry will have to take additional measures to fulfill the objectives set by the Cabinet. The administration of the State Border Service was also instructed to protect the state border of the Crimea and strengthen control at the exit from autonomy.
 