Man tortured by police to receive 27,000 euros of compensation

 
The European Court for Human Rights ruled to pay a compensation of 27,000 euros to Nizhny Novgorod resident Alexander Novoselov, who accused police officers of violence. In 2004, local police were investigating the assassination attempt of a prominent businessman, now the head of Nizhny Novgorod, Oleg Sorokin.
Human rights organization Committee Against Torture found that law enforcement officers kidnapped Novoselov, took him to the forest and tortured the man to make him confess in the attempt to assassinate the businessman. Later, the charges against the man were dropped, and everything that happened to Novoselov was explained as "an operational experiment," human rights activists said.
According to Nizhny Novgorod-based publication, Alexander Novoselov was a security guard of Mikhail Dikin, one of the brothers, who currently stays in prison for an attempted assassination of Oleg Sorokin committed at the end of 2003. 
 