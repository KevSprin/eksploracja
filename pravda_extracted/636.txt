Ukraine's illegal president signs making Yulia Tymoshenko election candidate

 
Alexander Turchynov, whom the Verkhovna Rada appointed Acting President and Parliament Speaker, signed the law, according to which former Ukrainian Prime Minister Yulia Tymoshenko and former Interior Minister Yuriy Lutsenko would be able to take part in the presidential election.
The Ukrainian Parliament adopted the law "On the rehabilitation of persons to implement decisions of the European Court of Human Rights" on February 28.
Based on the document, the persons, who have been convicted in the period from 1 February 2010 to 1 March 2014, and on whose complaints the European Court of Human Rights made rulings on violation of articles of the Convention for the Protection of Human Rights and Fundamental Freedoms, shall be considered rehabilitated.