Syria: Chaos, corruption, grand theft and an experiment

Syria's Chemical Weapons Destruction: Chaos, Corruption, Grand Theft and an Experiment
On 12th September last year, Syria's President al-Assad committed to surrender Syria's chemical weapons, with the caveats that the United States must stop threatening his country and supplying weapons to the terrorists. He has been as good as his word. The same cannot be said for the US and its boot licking allies.
Felicity Arbuthnot
Three days earlier US Secretary of State John Kerry - who had been killing Vietnamese in the US onslaught on Vietnam as American 'planes rained down 388,000 tons of chemical weapons on the Vietnamese people (i) - had threatened Syria with a military strike if the weapons stocks were not surrendered within a week, stating that President Assad: "isn't about to do it and it can't be done."
The ever trigger-happy Kerry was right on the second count, it can't be done for two reasons, extracting dangerous chemicals from a war zone is, to massively understate, a foolhardy and hazardous business. Additionally it seems having received Syria's agreement, the "international community" and the Nobel Peace Prize winning Organisation for the Prohibition of Chemical Weapons (OPCW) had no disposal plan in place and had not a clue what to do with them, whilst at every turn Syria is blamed.
As ever double standards and hypocrisy rule. According to CNN (10th October 2013): "The United States estimates it will be at least another decade before it completes destruction of the remaining 10% of its chemical weapons, estimated at more than 3,100 tons."  And Syria? "U.S. intelligence and other estimates put its chemical weapons stockpile at about 1,000 tons." they are believed to be "stored in dozens of sites", in the circumstances a logistical nightmare and a massive danger to the public and those driving them to be insisting on transporting them anywhere.
CNN also quotes Wade Mathews who had worked on "the U.S. project to destroy its chemical stockpile" who doubted that Syria could meet the deadlines. The US operation, he said: "took billions of dollars, the cooperation of many levels of government - including the military - and a safe environment to make sure the destruction was done safely."
"We had a coordinated effort, we had a government that insisted that it be done safely and that the community was protected ... I don't think those things are in place in Syria."
Having received Syria's compliance, the OPCW started shopping around for a country - any country it seems - to destroy the weapons. Norway, approached by the US, was first choice. They declined, since the country had no experience in dealing with chemical weapons, the Foreign Ministry website stating: "... Norway is not the most suitable location for this destruction." The second country approached was Albania, a request which the country's Prime Minister Edi Rama said also came direct from the United States.
According to the Berlin-based Regional Anti-Corruption Initiative, Albania is one of the most corrupt countries in Europe and the most corrupt in the Balkans, plummeting from a woeful 95 out of the 176 countries monitored in 2011, to 113 in 2012 and 116 in 2013, on their Corruption Perception Index.
In their end of year Report, the Initiative quotes Transparency International: "In Albania corruption is registering a new physiognomy in a favorable political environment, with characteristics like a new systems for money laundering, financing of political parties from illegal activities, the capture of the state through the control of procurement and privatization, human and narcotics trafficking and the impunity of high State officials before the justice system and the law."(ii)
Protestors against the weapons destruction took to the streets in thousands, some wearing gas masks and protective clothing, protests also took place in neighbouring Macedonia, with rallying outside the Albanian Embassy. 
Albania finally rejected with Rana apologetically grovelling to Washington: "Without the United States, Albanians would never have been free and independent in two countries that they are today", he said referring to Albania and Kosova and the massive March 24th1999 - June 10th1999 NATO and US assault on the former Yugoslavia with depleted uranium weapons which are, of course, both chemical and radioactive. A Science Applications International Report explains re the residue from the weapons: "Soluble forms present chemical hazards, primarily to the kidneys, while insoluble forms present hazards to the lungs from ionizing radiation ... short term effects of high doses can result in death, while long term effects of low doses have been implicated in cancer." 
In addition to concerns regarding corruption in Albania - terrorist groups would undoubtedly offer high sums for such weapons - safety might surely have been a consideration. In 2008 an explosion at an ammunition storage depot near Albania's capitol Tirana, killed twenty six people, wounded three hundred and damaged or destroyed five thousand five hundred homes. The disaster was said by investigators to be caused by a burning cigarette - in a depository for 1,400 tons of explosives.
Worse, when Albania was pressured to destroy its own chemical weapons stocks, some tons left over from the Cold War: "The U.S. offered to pay for their destruction and later hired some private company which destroyed the weapon capability of the chemicals but otherwise left a horrendous mess."
Hazardous waste was left in containers, on a concrete pad, inevitably they started to leak.
"In late 2007-early 2008, the US hired an environmental remediation firm, Savant Environmental, who determined the problem was worse than originally thought. Many of the containers were leaking salts of heavy metals, primarily arsenic, lead and mercury."
Moreover, the conexes - large, steel-reinforced shipping containers - were not waterproof, thus lethally contaminated condensation and water leakage dissolved some of the contaminants which leaked onto the ground.
"Savant Environmental repackaged the waste and placed it in twenty shipping containers. There it sits, visible from space", on the concrete pad - in the open.(iii)
All in all, why was Albania considered?
It is surely coincidence that on 3rd October last year, Tony "dodgy Iraq dossier" Blair, also an enthusiastic backer of Washington and NATO in their Balkans blitz, was appointed as advisor to the Albanian government to advise the impoverished country how to get in to the EU. Heaven forbid he might have advised that taking on lethal weapons no one else was prepared to touch, might tick quite a big approval box and made a call to someone somewhere in Washington. This is of course, entirely speculation.
 
However, as Pravda TV opined at the time, apart from the sorely needed financial boost: "It will increase the status and prestige of a poor country in Europe, Albania is in Europe's backyard, in this case it will be going foreground."(iv)
 
Belgium and France also declined an invitation to dispose of Syria's weapons, with Ralph Trapp, a consultant in disarming chemical weapons quoted as saying that "there remain very few candidates" for the task; "the hunt continues" commented The Telegraph (18th November 2013.)
 
The trail goes cold as to how many other governments may have been frantically begged to accept cargo loads of poisoned chalices as the US imposed clock ticked, but Italy caved in allowing around sixty containers to be transferred from a Danish cargo ship to a US ship in the Italian port of Giola Tauro, in Calabria, with further consignments also expected to arrive. 
 
The permission caused widespread demonstrations in Southern Italy, the government accused of secrecy and one demonstrator summing up the prevailing mood: "They are telling us that the material carried is not dangerous, but in fact nobody knows what is inside those containers." Not dangerous eh? Does any government, anywhere ever tell the truth?
 
The Giola Tauro port, which accounts for half the Calabria region's economy "has been in crisis since 2011", with four hundred workers on temporary redundancies - out of a total workforce of thirteen hundred. Not too hard to arm twist, the cynic might think.
 
The port also suffers from allegations of being a: " major hub for cocaine shipments to Europe by the Calabria-based 'Ndrangheta mafia." However, Domenico Bagala, head of the Medcenter/Contship terminal where the operation is planned countered with: "Since Gioia Tauro handles around a third of the containers arriving in Italy, it is normal that it has more containers that are seized", adding: "We operate in a difficult territory but we have hi-tech security measures in place." 
 
Calabria is, in fact, plagued by corruption and organized crime. A classfied cable from J. Patrick Truhn, US Consul General in Naples (2nd February 2008) obtained by Wikileaks stated: "If it were not part of Italy, Calabria would be a failed state.  The 'Ndrangheta organized crime syndicate controls vast portions of its territory and economy, and accounts for at least three percent of Italy's GDP (probably much more) through drug trafficking, extortion and usury."  Further: "During a November 17-20 visit to all five provinces, virtually every interlocutor painted a picture of a region ... throttled by the iron grip of Western Europe's largest and most powerful organized crime syndicate, the 'Ndrangheta."(v)
 
Moreover: "The 'Ndrangheta is the most powerful criminal organization in the world with a revenue that stands at around fifty three billion Euros (seventy two billion U.S. dollars - forty four billion British pounds)" records Wikipedia, noting operations in nine countries, on four continents. Arguably, a less ideal transit point than Calabria for a stockpile of chemical weapons would be hard to find.
 
Of special concern to Carmelo Cozza of the SUL trade union is the port's neighbouring village of San Ferdinando which has protested the operation: "The schools are right next door!"(vi)
 
However, when it comes to dodgy dealings, organized crime could seemingly learn a thing or two from the EU. Large amounts of Syria's financial assets, frozen by the European Union, have simply been spirited from accounts, in what the Syrian Foreign Ministry slams as: "a flagrant violation of law."
 
Last week the EU endorsed the raiding of Syria'a financial assets frozen across Europe and the the transfer of funds to    " ... the Organization for the Prohibition of Chemical Weapons (OPCW) ... a flagrant violation of the international law and the UN Charter and understandings reached by the executive board of the OPCW", commented a Foreign Ministry source, adding: "the European step violates the resolution of the OPCW executive board adopted on 15th November 2013 which acknowledged Syria's stance which was conveyed to the Organization, officially stating the inability to shoulder the financial costs of destroying the chemical weapons."
 
The theft of Syria's moneys was condemned as a: "swindle policy practiced by some influential countries inside the EU at a time when they reject to release frozen assets to fund purchase of food and medicine which is considered the priority of the Syrian state ... (meanwhile) the EU allowed its members to arm the terrorist groups which are responsible for bloodshed in Syria ... " the source added."(vi) It is hard to disagree.
 
The EU/UN/OPCW has apparently learned well from the UN weapons inspectors and other UN benefits from the Iraq embargo, which bled the country dry from "frozen" assets, to which they helped themselves, as the children died at an average of six thousand a month year after year, from "embargo related causes." As the UN spent Iraq's moneys, Iraq's water became a biological weapon, the lights went off and medical and educational facilities largely collapsed. Are UN embargoes the UN's shameful new money spinner?
 
So, can things get worse in the black farce which is the chaotic, dangerous, disorganised disposal attempts of Syria's chemical materials? You bet they can. The companies selected to destroy the chemicals are Finland's Ekokem and the US subsiduary of the French giant Veolia.
 
"The most dangerous materials are to be neutralized at sea by the Cape Ray, an American naval vessel specially outfitted for that purpose, which departed its Norfolk, Va., home port on Jan. 27 for the Mediterranean." (New York Times, 14th February 2014.) A method which has never been tried before, an experiment seemingly to take place in the Mediterranean, not in US territorial waters. "It's Not Just a Job, It's An Adventure", was a US Navy recruiting slogan. Doubt the population of the countries bordering the near enclosed Mediterranean feel quite the same, from Europe to Anatolia, North Africa to the Levant.
 
Additionally, the inclusion of Veolia as a suitable partner in the whole dodgy venture is in a class of its own. The company has long been involved in waste management and vast transport projects in the illegal settlements in Israel.
 
In November 2012 Professor Richard Falk, wrote, on UN note paper, to the (UK) North London Waste Authority who were considering awarding £4.7 billion worth of contracts to Veolia. His letter(viii) quoted in part below, detailing his concerns regarding the company's compliance with international legal norms, speaks for itself:
 
"I am writing to you in my capacity as the United Nations Special Rapporteur on the situation of human rights in the Palestinian territories occupied since 1967 to urge you not to select Veolia for public contracts due to its active involvement in Israel's grave violations of international law.
 
"Due to its deep and ongoing complicity with Israeli violations of international law and the strength of concern of Palestinian, European and Israeli civil society about the role played by Veolia, I decided to select Veolia as one of the case studies to include in my report. I have attached the report for your consideration.
 
"Veolia is a signatory to the UN Global Compact, a set of principles regarding business conduct. Yet its wide ranging and active involvement in Israel's settlement regime and persistent failure to exercise due diligence show utter disregard for the human rights related principles of the Global Compact.
 
"It is my view that Veolia's violations of the UN Global Compact principles and its deep and protracted complicity with grave breaches of international law make it an inappropriate partner for any public institution, especially as a provider of public services."
 
Professor Falk concludes:
 
"I urge you to follow the example set by public authorities and European banks that have chosen to disassociate themselves from Veolia and take the just and principled decision not to award Veolia any public service contracts. Such a measure would contribute to upholding the rule of law and advancing peace based on justice."
 
So a company in breach of international law is being awarded a contract to a UN body (the OPCW) in spite of being condemned by a distinguished UN legal expert and Special Rapporteur. 
 
The final anomaly, for now, as Bob Rigg - former UN weapons inspector in Iraq, and former senior editor for the OCPW and former Chair of the New Zealand National Consultative Committee on Disarmament - points out:
 
 "At present, Israel has a monopoly on nuclear weapons in the middle east. Once the destruction of Syria's chemical weapons is complete, Israel will enjoy a near regional monopoly over a second weapon of mass destruction -chemical weapons. In addition to Israel, Egypt is the only regional power with a chemical-weapons capability. "
 
At all levels, law breakers rule supreme.
 
i. http://www.ingeniouspress.com/2013/09/06/u-s-armed-forces-dropped-388000-tons-of-napalm-on-vietnam-napalm-is-a-chemical-weapon/
ii. http://www.anticorruption-albania.org/
iii. http://whatsupic.com/news-politics-usa/1384669037.html
iv. http://www.youtube.com/watch?v=TYCF8ABACvU (1.12 secs.)
v. http://wikileaks.org/cable/2008/12/08NAPLES96.html
vi. http://www.thelocal.it/20140212/syrian-arms-arrival-could-boost-italian-port
vii. http://syria360.wordpress.com/2014/02/11/foreign-ministry-release-of-syrian-assets-frozen-by-eu-to-fund-elimination-of-chemical-weapons-flagrant-violation-of-law/
viii. http://www.dumpveolia.org.uk/wp-content/uploads/2012/12/UN_Falk-letter_2012_11_16_Veolia.pdf
 
 