Russian PM Medvedev admits legitimacy of 'virtually worthless' Viktor Yanukovych

 
Russian Prime Minister Dmitry Medvedev published an entry on his Facebook page admitting the legitimacy of President Viktor Yanukovych.  However, Russian Prime Minister described the credibility of the Ukrainian leader as "virtually worthless," which, according to Medvedev, does not negate Viktor Yanukovych's right to serve as the constitutionally elected president.
"If he is guilty before Ukraine - then conduct the impeachment procedure in accordance with the Constitution of Ukraine, (Article 111) and try him," Dmitry Medvedev wrote.  The Article of the Criminal Code of Ukraine, to which the Russian Prime Minister referred to (high treason) , is punishable by imprisonment from 10 to 15 years.