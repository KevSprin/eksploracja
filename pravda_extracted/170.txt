Abuse of Russian children in USA not over

 
Russian officials requested the U.S. Attorney General to investigate incidents of rights violations of 26 adopted Russian children. The statement, containing the facts of journalistic investigation conducted by Reuters and NBC, was signed by the head of the Investigative Committee Alexander Bastrykin.
In an official letter to Eric Holder, Bastrikin expressed hope for cooperation "on the issue, which, unfortunately, still has not lost its relevance."
This time, "it goes about Anna Barnes (born Anna Faizzulina in 1994), Inga Whatkott (Inga Kurasova, born in 1985 ), Dmitry Stewart, and 23 more Russian children."
Bastrykin also recalled that "the issues related to attempts against life and health of underage Russian children adopted by U.S. citizens, have been discussed during the Russian-American talks on various levels."
 
 