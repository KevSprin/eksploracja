Russian freestyler, who broke her spine in Sochi, receives second surgery

 
Freestyler Maria Komissarova, who broke her spine during training at the Olympic Games in Sochi, received second surgery.
The girl is being treated at the University Hospital in Munich, where the athlete was taken on a special flight from Sochi. Her father, Leonid Komissarov, said that the surgery was successful, but there was another surgery due next week.  The clinic, where Komissarova is being treated, provided medical treatment to bobsledder Irina Skvortsova, who was at the opening of the Games in Sochi next to Russian president.