Two Ukrainians wounded defending Lenin monument near Odessa, Ukraine

 
Two Ukrainians from the town of Iljichiovsk in the Odessa region of Ukraine were injured from traumatic weapons, trying to defend the statue of Lenin.
At night of February 23, a large number of local residents gathered on Strebko Square. The people came to defend the monument to Vladimir Lenin from the opposition.
At about two o'clock in the morning, a conflict sparked between two residents. A man, born in 1978, fired a traumatic weapon at two other residents. Both victims were taken to hospital.
The police opened a criminal case under article "Hooliganism" of the Criminal Code of Ukraine.
Earlier, a statue of Lenin was demolished in Dnepropetrovsk on Sunday.
 