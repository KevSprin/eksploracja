Anzhi out of Europa League

Russia's last representative in Europe, Anzhi Makhachkala, was eliminated from the Europa League by AZ Alkmaar, after a goalless draw at home followed a defeat away in the Netherlands by one goal to nil. Seven out of the sixteen teams left in the UEFA competitions are from the Iberian Peninsula. Benfica and FC Porto join the five Spanish teams.
The real excitement of the day came in the Spanish city of Seville, where Betis faced rivals Sevilla in the local derby. In the first leg, Betis won 2-0 away; today Sevilla won 2-0 away. Extra time did not break the impasse, and the game went to penalties... which Sevilla won 4-3!
Europa League: Round of 16
Anzhi Makhachkala 0 AZ Alkmaar 0
First leg:
AZ Alkmaar (Netherlands) 1 Anzhi Makhachkala (Russia) 0
Aggregate: Alkmaar 1-0
  
Benfica 2 Tottenham 2
First leg:
Tottenham (England) 1 Benfica (Portugal) 3
Aggregate: Benfica 5-3
 
Fiorentina 0 Juventus 1
First leg:
Juventus (Italy) 1 Fiorentina (Italy) 1
Aggregate: Juventus 2-1
 
Napoli 2 FC Porto 2
First leg:
FC Porto (Portugal) 1 Napoli (Italy) 0
Aggregate: FC Porto 3-2
 
Red Bull Salzburg 1 Basel 2
First leg:
Basel (Switzerland) 0 Red Bull Salzburg (Austria) 0
Aggregate: Basel 2-1
 
Valencia 1 Ludogorets Razgrad 0
First leg:
Ludogorets Razgrad (Bulgaria) 0 Valencia (Spain) 3
Aggregate: Valencia 4-0
  
Viktoria Plzen 2 Lyon 1
First leg:
Lyon (France) 4 Plzen (Czech Republic) 1
Aggregate: Lyon 5-3
  
Betis 0 Sevilla 2
First leg:
Sevilla (Spain) 0 Betis (Spain) 2
Extra time: no score. Sevilla won 4-3 on penalties
Aggregate: Sevilla, 2-2, 4-3 pen.
  
  
  
  
Previous round: Games played on February 20 and 27
First leg first, second leg second and aggregate (AGG)
  
Dnipro (UKR) 1 v Tottenham (ENG) 0
Tottenham Hotspur 3 Dnipro 1 AGG: Tottenham 3-2
  
Betis (ESP) 1 v Rubin (RUS) 1
Rubin Kazan 0 Real Betis 2 AGG: Betis 3-1
  
Swansea (English League - WALES) 0 v Napoli (ITA) 0
Napoli 3 Swansea 1 AGG: Napoli 3-1
  
Juventus (ITA) 2 v Trabzonspor (TUR) 0
Trabzonspor 0 Juventus 2 AGG: Juventus 4-0
  
Maribor (SVN) 2 v Sevilla (ESP) 2
Sevilla 2 Maribor 1 AGG: Sevilla 4-3
  
Plzeň (CZE) 1 v Shakhtar Donetsk (UKR) 1
Shakhtar Donetsk 1 Viktoria Plzeň 2 AGG: Plzeň 3-2
  
Chornomorets Odesa (UKR) 0 v Lyon (FRA) 0
Lyon 1 Chornomorets Odesa 0 AGG: Lyon 1-0
  
Lazio (ITA) 0 v Ludogorets (BUL) 1
Ludogorets Razgrad 3 Lazio 3 AGG: Ludogorets 4-3
  
Esbjerg (DEN) 1 v Fiorentina (ITA) 3
Fiorentina 1 Esbjerg 1 AGG: Fiorentina 4-2
  
Ajax (NED) 0 v Salzburg (AUT) 3
Red Bull Salzburg 3 Ajax 1 AGG: Red Bull Salzburg 6-1
  
M. Tel-Aviv (ISR) 0 v Basel (SUI) 0
Basel 3 Maccabi Tel-Aviv 0 AGG: Basel 3-0
  
FC Porto (POR) 2 v Eintracht Frankfurt (GER) 2
Eintracht Frankfurt 3 FC Porto 3 AGG: FC Porto 5-5
  
Anji (RUS) 0 v Genk (BEL) 0
Genk 0 Anzhi Makhachkala 2 AGG: Anzhi 2-0
  
Dynamo Kyiv (UKR) 0 v Valencia (ESP) 2
Valencia 0 Dynamo Kyiv 0 AGG: Valencia 2-0
  
PAOK (GRE) 0 v Benfica (POR) 1
Benfica 3 PAOK 0 AGG: Benfica 4-0
  
Liberec (CZE) 0 v AZ (NED) 1
AZ Alkmaar 1 Slovan Liberec 1 AGG: AZ 2-1
 Teams remaining
 France: Lyon
Italy: Juventus
Netherlands: AZ Alkmaar
Portugal (2): Benfica, FC Porto
Spain (2): Sevilla, Valencia
Switzerland: Basel 
Champions League
Borussia Dortmund 1  FC Zenit 2
Kehl 36                          Hulk 16 Rondón 73
First leg
FC Zenit 2 Borussia Dortmund 4
Aggregate: Dortmund 5-4
 
Borussia Dortmund won the tie 5-4 on aggregate after both legs. Having won in Saint Petersburg, the German team concentrated on controlling the game at home, giving away more of the ball possession (48% to 52%) but had more of the play, translated into 12 shots (3 on) as against 8 shots for Zenit, 5 on and the home team had 8 corners compared to Zenit's 1.
 
  
Manchester United 3 Olympiacos 0
Van Persie 25′ (pen),
45′, 51′
First leg
Olympiacos FC 2 Manchester United FC 0
Aggregate: Manchester United 3-2
 
David Moyes sees his Manchester United team enter the next round of the Champions League, overturning the first leg result in Greece, in which Olympiacos won 2-0. The Van Persie hat-trick did the trick on the day and helps Moyes in his quest to remain in charge at United.
  
Chelsea FC 2  Galatasaray AŞ 0
Eto'o 4
Cahill 42
First leg
Galatasaray AŞ 1 Chelsea FC 1 
Aggregate: Chelsea FC 3-1
 
The match statistics speak for themselves: Chelsea FC managed the game and the leg from beginning to end, sharing ball possession by 49% to 51% for Galatasaray but Chelsea had 20 shots, nine on target against Galatasaray's three, none on. Chelsea had seven corners to zero for the visitors.
  
Real Madrid 3 FC Schalke 04 1
Ronaldo 21', 74'
Morata 75'
First leg
FC Schalke 04 1 Real Madrid CF 6
Aggregate: Real Madrid 9-2
  
Real Madrid had the game buried from the first leg after the phenomenal result away in Germany (6-1). Tonight Real had a ball possession of 57% against Schalke's 43% and had far more of the action in terms of shots, 23 and 10 on target, while Schalke managed 14 shots, three on. Real also had nine corners compared with Schalke's 2.
  
Previous games in this round
  
FC Barcelona 2 Manchester City FC 1
Messi 67,            Kompany 89
Dani Alves 90
First leg
Manchester City FC 0 FC Barcelona 2
Aggregate: Barcelona 4-1
  
The damage had already been done by Messi on 67', before Kompany got City's equalizer but with one minute to go, the Mancunians needed three more goals to win the tie and a minute later, Dani Alves shot home from the middle of the box. Barça had more possession, 57% to 43% and more shots 15 (7 on) to 11 (4 on).
  
Paris Saint-Germain 2 Bayer 04 Leverkusen 1
Marquinhos 13            Sam 6
Lavezzi 53
First leg
Bayer 04 Leverkusen 0 Paris Saint-Germain 4
Aggregate: PSG 6-1
 
Sam started off well for the Germans but by half-time, Marquinhos had imposed the foul-goal advantage, which was to be five on 53', through Lavezzi. PSG had 62% of ball possession to 38% for the visitors. Both teams had 5 shots on target, PSG having 14 in total to Leverkusen's 9.
  
Club Atlético de Madrid 4 AC Milan 1
Diego Costa 3′, 85′                Kaká 27
Turan 40′
Raúl García 70′      
                                       
First leg
AC Milan 0 Club Atlético de Madrid 1
Aggregate: Atlético goes through 5-1
 
The early goal by Diego Costa sent a shiver down AC Milan's spine but Kaká pulled the game back with a headed goal on 27 minutes. However by the break Atlético was ahead and added two more in the second half. AC Milan is a shadow of its former self, Atlético looking very much a new boy on the block. Curiously, AC Milan enjoyed slightly more ball possession (53% against 47%). Atlético had 13 shots (6 on) against Milan's 11 (3) and there were three corners apiece.
  
FC Bayern Munchen 1 Arsenal FC 1
Schweinsteiger 54'       Podolski 57'
 
First leg
Arsenal FC 0 FC Bayern München 2
Aggregate: Bayern goes through 3-1
 
Bastian Schweinsteiger scored the first on 54', Lukas Podolski equalizing three minutes later. Bayern had 67% ball possession to Arsenal's 33% and had 14 shots to Arsenal's 7, six on target against 3.
Teams remaining:
England (2): Chelsea FC, Manchester United
Spain (3): Atlético de Madrid, FC Barcelona, Real Madrid
Germany (3): Bayern Munchen, Borussia Dortmund
France (1): Paris Saint-Germain
 Total number of teams in European competitions:
Champions League + Europa League = Total
Spain 3 + 2 = 5
Portugal: 0 + 2 = 2
England: 2 + 0 = 2
France: 1 + 1 = 2
Germany: 2 + 0 = 2
Italy: 0 + 1 = 1
Netherlands: 0 + 1 = 1
Switzerland: 0 + 1 = 1
Champions and Europa League draws
The matches are drawn for the last 16 clubs of the Champions League and the last 32 of the Europa League The Champions League matches will take place on February 18/19 and 25/26 and March 11/12 and 18/19. The Europa League games will be on February 20 and 27.
Europa League: Quarter-finals on April 3 and 10; Semi-finals on April 24 and May 1; Final on May 14
Champions League: Quarter-finals on April 1 and 2 and April 8 and 9; Semi-finals on April 22 and 23 and April 29 and 30; Final on May 24
Timothy Bancroft-Hinchey
Pravda.Ru