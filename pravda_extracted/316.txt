Worshippers stand 9 hours in lines to worship Gifts of Magi in Moscow

 
At least 20 thousand people came to the Cathedral of Christ the Savior in Moscow to worship the Gifts of the Magi brought from Mount Athos. The relics are directly related to the birth of Jesus Christ.  Many pilgrims are ready to stand in the line for more than nine hours to touch the relics. Authorities urge parishioners to rely on their own forces and not to use children as an excuse not to stand in the line for hours.
The relic will stay in the Cathedral of Christ the Savior through January 13 and is available for citizens every day from 8 a.m. to 10 p.m. Seven food points were organized for pilgrims, 210 toilets were set and 23 buses were provided, where they can rest and get warm.
The Gifts of the Magi are gold, frankincense and myrrh that the Wise Men brought to the cradle to Jesus Christ when the baby was born.
From Moscow, the holy relics will be taken to St. Petersburg on January 14. The gifts are traditionally kept on Mount Athos in Greece for centuries.