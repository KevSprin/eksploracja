Chaos in Ukraine: Civilians killed, death toll grows

As many as 30,000 gathered on Kiev's Maidan in the morning of February 20. Several civilians have been killed in violent clashes on Thursday, although it is not clear by whom the people were killed. According to various sources, as least 20 people have been killed on Feb. 20.
Ukrainian UNIAN news agency said citing local journalists that there were 13 dead bodies in Ukraine Hotel, near Maidan.
Also read: Warplanes spotted above Kiev
According to other sources, only seven people have been killed. Representatives of Maidan activists said that these people were shot dead by police snipers, who allegedly aimed at the head.
Ukraine's Ministry of Health officially confirmed the information on seven victims. Spokespeople for the Ministry of Internal Affairs reported that on February 20, three police officers were killed, at least 50 were injured. In total, 35 have been killed after the fighting in Kiev resumed, the Health Ministry reported.
Also read: Civil war in Ukraine
In turn, Interior Ministry spokesman Sergei Burlakov said on local television that there was a toxic substance used against the police.
In pictures: Kiev on fire
"Law enforcement did not use and will not use weapons against civilians. In response to attempts of the extremists to harm the life and health of police officers, the police will respond in accordance with the law of Ukraine "About Police," officials with Ukraine's Interior Ministry said.
On February 20, a day of mourning was declared in Ukraine to pay tribute to those who were killed in violent clashes.
Many Ukrainian news agencies conduct online text and photo broadcast of the developing events in Kiev. UNIAN reports that protesters set fire to a water cannon vehicle on Maidan.
Berkut soldiers left positions near October Palace that protesters seized again. Demonstrators went on the offensive, firing their guns at police and throwing rocks at them.
Also read: Russia ready to provide military help to Ukraine, if Yanukovych asks
UPDATE: According to most recent information, more than 50 people have been killed in Kiev today, Valeria Lutkovskaya, a Commissioner for Human Rights of the Verkhovna Rada said.
In the course of the fighting, extremists took 67 officers of Interior Ministry troops prisoners. Their fate and condition remains unknown. 
Local authorities allowed law enforcers to use combat weapons to "release their colleagues."
Radicals have been using firearms for a long time already, including automatic and sniper rifles.
Kiev is being looted, a local museum has been robbed.