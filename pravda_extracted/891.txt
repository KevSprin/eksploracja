Russia successfully tests RS-12M Topol ICBM

 
On Kapustin Yar range ground in the Astrakhan region, Russia tested RS-12M "Topol" intercontinental ballistic missile.  According to the press service of the Strategic Missile Forces of the Defense Ministry, the launch took place at 22:10. The missile successfully hit the training target in Kazakhstan, located on Sary-Shagan range ground.
Officials from the Defense Ministry of the Russian Federation explained that the goal of the launch was to test the prospective combat equipment of intercontinental ballistic missiles. It was also said that Kapustin Yar range ground was a unique site as it is only test trajectories and measurement system of this particular range ground make such tests possible.
 
 