Russian Foreign Ministry finds decisions of Ukrainian Parliament illegal

 
Russia's Foreign Ministry has published an official statement, in which the most recent actions of the Verkhovna Rada of Ukraine were found illegitimate.
The statement runs that Ukraine now "stamps" the laws violating the right of national minorities, including Russians. The Rada tries to ban the Russian language, non-loyal parties and media outlets, propagandizing Nazi values. All this is being down with reference to revolutionary reforms.
The ministry reminded that Ukraine threatens Russian-speaking individuals and conducts a struggle against monuments of Russian culture.
Earlier, the foreign ministry has expressed concerns about the desecration of monuments in Ukraine, pointing out the demolition of the monument to Kutuzov in the town of Brody on February 25th and the desecration of memorials to Soviet warriors.
Russian Foreign Minister Sergei Lavrov also condemned the decision of the Rada to hold early presidential election on May 25th. To crown it all, the head of the ministry stated that the move to ban the broadcast of Russian TV channels in Ukraine would be a violation of freedom of speech. Russian Prime Minister Dmitry Medvedev released similar statements yesterday.
 