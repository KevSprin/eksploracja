Russians attack Polish embassy in Moscow

 
The Moscow police detained three young men, who organized an attack on the embassy of Poland in the Russian capital. According to police, the hooligans tried to throw burning flares at the embassy, but failed to throw them over the fence.
The three perpetrators were detained and taken to the nearest police station. Natives of the city of Voronezh, 22-year-old Sergei Zaplavnov, 30-year-old Konstantin Makarov and 27-year-old Simon Verdinyan turned out to be members of Eduard Limonov's Other Russia movement.
Polish diplomats asked the Moscow authorities to strengthen security and protection for the Polish embassy in the capital, fearing large-scale attacks.
Meanwhile, Polish President Bronislaw Komorowski apologized to Russia for the unrest at the Russian embassy in Warsaw.
He said in a radio broadcast that the events from November 11 could not be justified, and one could only apologize for them.
The March of Independence was held in Warsaw during the celebration of the 95th anniversary of Poland's independence. 
 