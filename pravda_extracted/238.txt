Gold falls against US dollar

 
On November 7, the price of gold fell as the U.S. currency strengthened against the euro after the ECB lowered the basic interest rate. The newly released information on U.S. GDP also played its role.
December gold futures on the New York Comex Exchange fell by 9.3 dollars, or 0.7% - to 1,308.5 dollars per ounce.
The European Central Bank unexpectedly lowered the benchmark interest rate to 0.25% from 0.5%. The price of the precious metal was influenced by U.S. GDP data for the third quarter of 2013. According to preliminary estimates, the figure was 2.8% in annual terms, while analysts expected a GDP growth of 2%.
 