Russia to give Belarus $2bn loan in 2014

 
In 2014, Russia will give Belarus a loan of two billion dollars, President Vladimir Putin said.
According to the president, the decision to give the loan to the country is based on the current trends on world markets. The decision was made after the meeting of the Supreme State Council of Russia and Belarus.
In December, Moscow decided to allocate $15 billion from the National Welfare Fund to Ukraine. Financial assistance was provided to the Ukrainian side in view of the serious condition in the Ukrainian economy.
 