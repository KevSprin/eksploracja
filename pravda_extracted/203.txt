Putin and Medvedev discuss Volgograd blasts

 
President Vladimir Putin and Prime Minister Dmitry Medvedev discussed assistance to those, who were either killed of injured as a result of the bombings in Volgograd.
Putin and Medevedev discussed a full range of issues related to the provision of medical, financial and other forms of assistance to support the people who were affected in the attacks.
It was reported that in connection with terrorist attacks at the railway station and in a trolleybus in Volgograd on December 29 and 30, Vladimir Putin conducted a meeting with Interior Minister Vladimir Kolokoltsev and the head of the Federal Security Service Alexander Bortnikov.
 
After the meeting, Bortnikov was instructed to fly to Volgograd.
 
 