Super Cristiano

Who can say Cristiano Ronaldo is not the best player in the world at present after his four-goal tally in two games, providing the FIFA World Cup in Brazil with its second Portuguese-speaking team? To say tonight's hat-trick in Solna took Portugal through would be to belie the effort of Paulo Bento's squad, a certain contender for the title.
What a match between Sweden and Portugal at the Friends'Arena in Solna, metropolitan Stockholm, with Zlatan Ibrahimovic neutralizing Portugal's goal in Lisbon and the first by Cristiano Ronaldo in Sweden with two goals on 68 and 72'. A brace from Ronaldo in two minutes (77 and 79') put the match well beyond the hosts and books Portugal's place in the World Cup Finals 2014 in Brazil.
Iceland was 90 minutes away from the finals for the first time in its footballing history, but goals from Mandzukic and Srna saw the better side go through. Croatia flies alongside Portugal, France and Greece.
As for France, Deschamps' side overturned a 2-0 defeat in Ukraine and France was level by the break, goals from Sakho and Benzema. An own goal from Oleg Gusev in the second half buried Ukraine's hopes.
Fernando Santos, the Portuguese coach, takes Greece to its second successive World Cup Finals, with a 1-1 away draw in Bucharest after winning 3-1 at home. Both goals tonight were scored by Greeks - Mitroglou got his third in two games on 23' and Torosidis scored an own goal after the break.
Sweden 2 Portugal 3 Aggregate: 2-4 Portugal qualifies
Zlatan Ibrahimovic (2)  Cristiano Rolando (3)
(68, 72)                          (50, 77, 79)
Croatia 2 Iceland 0 Aggregate 2-0 Croatia qualifies
Mario Mandzukic (27)
Darijo Srna (47)
France 3 Ukraine 0 Aggregate 3-2 France qualifies
Mamadou Sakho (22)
Karim Benzema (34)
Oleg Gusev (o.g.) (72)
Romania 1 Greece 1 Aggregate 2-4 Greece qualifies
Vasileios Torosidis (o.g.) (55)  Konstantinos Mitroglou (23)
First legs
The first round of the play-offs for qualification for the FIFA 2014 World Cup Finals in Brazil took place on Friday in the UEFA zone. Here are the results.
Iceland 0 Croatia 0; Portugal 1 Sweden 0; Ukraine 2 France 0; Greece 3  Romania 1
Portugal 1 Sweden 0
Cristiano Ronaldo
Iceland 0 Croatia 0
Ukraine 2 France 0
Roman Zozulia, Andriy Yarmolenko
Greece 3 Romania 1
Konstantinos Mitroglou (2),   Bogdan Stancu
Dimitrios Salpingidis
 
Timothy Bancroft-Hinchey
Pravda.Ru
 