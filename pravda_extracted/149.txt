USA approves billion-dollar loan for Ukraine

 
On March 6, the U.S. House of Representatives approved a bill to support loan guarantees of $1 billion for the new Ukrainian government.  As many as 385 people voted "for" bill, 23 - voted "against." The U.S. Senate will soon consider another similar document, which, when approved, will be sent to the White House for President Barack Obama to sign.
The package of bills is to play a supporting role in relation to a much broader assistance program that the U.S. lobbies at the International Monetary Fund.  The purpose of the package, in particular, is to reduce Ukraine's dependence on natural gas supplies from Russia.
 