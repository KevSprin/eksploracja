Champions League: Matchday 6 complete - Zenit through

Chelsea, Schalke, Dortmund, Arsenal, Atlético Madrid, Zenit Saint Petersburg, Barcelona, AC Milan join Manchester United, Leverkusen, PSG, Olympiacos, Real Madrid, Galatasaray, Bayern Munich, Manchester City, through to the knock-out phase of the Champions League.
The following teams continue in the Europa League: Shakhtar Donetsk, SL Benfica, Plzen, Juventus, Basel, Napoli, FC Porto, Ajax.
The first two teams in each group carry on to the knock-out phase of the Champions League. The teams in third place proceed in the Europa League. The last places drop out of UEFA European competitions this season.
Group E
Chelsea 1 Steaua Bucharest 0
Ba 10
Schalke 2 Basel 0
Draxler 50
Matpi 57
After 6 games
Chelsea 12, Schalke 10 points, Basel 8, Steaua 3
Group F
Marseille 1  Dortmund 2
Diawara 14  Lewandowski 4, Grosskreutz 87
Napoli 2 Arsenal 0
Higuain 73
Callejon 90
After 6 games
Dortmund 12, Arsenal 12, Napoli 12, Marseille 0
Group G
Austria Wien 4 Zenit Saint Petersburg 1
Hosiner 44, 51  Kerzhakov 35
Jun 48
Kienast 90
Atlético Madrid 2 FC Porto 0
R. Garcia 14
D. Costa 37
After 6 games
Atlético 16, Zenit 6, FC Porto 5, Austria Wien 5
Group H
Barcelona 6 Celtic 1
Piqué 7    Samaras 88
Pedro 39
Neymar 44, 48, 54
Tello 72
AC Milan 0 Ajax 0
After 6 games
Barcelona 12, AC Milan 9, Ajax 8, Celtic 3
 
Group A
Manchester United 1 Shakhtar Donetsk 0
Jones 67
Real Sociedad 0 Bayer Leverkusen 1
                           Ömer Toprak 49
After 6 games
Manchester United 14 points, Bayer Leverkusen 10, Shakhtar 8, Real Sociedad 1
 
Group B
SL Benfica 2 Paris Saint-Germain 1
Lima 45 (pen)  Edinson Cavani 37
Gaitán 58
Olympiacos 3 Anderlecht 1
Saviola 33, 58 Kljestan 39
Domínguez 90+4 (pen)
 
After 6 games
PSG 13, Olympiacos 10, Benfica 10, Anderlecht 1
 
Group C  
Kobenhavn 0 Real Madrid 2
                      Modric 25, Ronaldo 48
Galatasaray 1 Juventus 0
After 6 games
Real Madrid 16, Galatasaray 7, Juventus 6, Kobenhavn 5
 
Group D
Bayern Munich 2 Manchester City 3
T. Müller 5           Silva 28, Kolarov 59 (pen), Milner 62
Götze 12
Plzen 2 CSKA Moskva 1
Kolar 76  Musa 65
Wágner 90
 
After 6 games
Bayern Munich 15, Manchester City 15, Plzen 3, CSKA Moskva 3,
 
 
 
Previous round
  
Group A
Shakhtar Donetsk 4 Real Sociedad 0
L.Adriano 37
Teixeira 48
D. Costa 68, 87
Leverkusen 0 Manchester United 5
                        Valencia 22, Spahic 30 (o.g.), Evans 65, Smalling 77, Nani 88
After 5 games
Manchester United 11 points, Shakhtar 8, Bayer Leverkusen 7, Real Sociedad 1
  
Group B
Real Madrid 4 Galatasaray1
Bale 37,            Bulut 38
Arbeloa 51,
Di Maria 63,
Isco 80
Juventus 3                            Kobenhavn 1
Vidal 29 (pen) 61 (pen)       Mellberg 56
63
After 5 games
Real Madrid 13, Juventus 6, Kobenhavn 5, Galatasaray 4,
 
Group C
PSG 2                Olympiacos 1
Ibrahimovic 7    Manolas 80
Cavani 90
Anderlecht 2           Benfica 3
M. Mangulu 18       Matic 34, M.Mangulu (52 o.g.), Rodrigo 90
Bruno 76
After 5 games
PSG 13, Olympiacos 7, Benfica 7, Anderlecht 1
 
Group D
CSKA Moskva 1 Bayern Munich 3
Honda 62 (pen)   Robben 17, Götze 56, T. Müller 65 (pen)
Manchester City 4 Plzen 2
Aguero 33 (pen)    Horava 43, Tecl 69
Nasri 65
Negredo 78
Dzeko 89
After 5 games
Bayern Munich 15, Manchester City 12, CSKA Moskva 3, Plzen 0
 
Yesterday's games
 
Group E
Basel 1 Chelsea FC 0
Salah 87
Steaua Bucharest 0  Schalke 0
After 5 games
Chelsea 9, Basel 8, Schalke 7 points, Steaua 3
 
Group F
Arsenal 2 Marseille 0
Wilshere 1, 65
Dortmund 3 Napoli 1
Reus 10 (pen) Insigne 71
Blaszczykowski 60
Aubameyang 78
After 5 games
Arsenal 12, Napoli 9, Dortmund 9, Marseille 0
 
Group G
FC Porto1        Austria Wien 1
J. Matinez 48  Kienast 11
Zenit 1                         Atlético Madrid 1
Alderweireld 74 (o.g.) Adrián López 53
After 5 games
Atlético 13, Zenit 6, FC Porto 5, Austria Wien 2
 
Group H
Celtic 0 AC Milan 3
              Kaká 13; Zapata 49; Balotelli 60
Ajax 2 Barcelona 1
Serero 19 Xavi Hernández 49 (pen)
Hoesen 42
After 5 games
Barcelona 12, AC Milan 8, Ajax 7, Celtic 3
 
Round 4
 
Group A
 
Real Sociedad 0   Manchester United 0
Shakhtar Donetsk 0  Bayer Leverkusen 0
 
After 4 games
Manchester United 8 points, Bayer Leverkusen 7, Shakhtar 5, Real Sociedad 1
 
Group B
 
Juventus 2 Real Madrid 2
Vidal,        C. Ronaldo, Bale 
Llorente
Kobenhavn 1  Galatasaray 0
Braaten
 
After 4 games
Real Madrid 10, Kobenhavn 5, Galatasaray 4, Juventus 3,
 
Group C
PSG 1             Anderlecht 1
Ibrahimovic   de Zeeuw
 
Olympiacos 1 Benfica 0
Manolas
 
After 4 games
PSG 10, Olympiacos 7, Benfica 4, Anderlecht 1
 
Group D
Plzen 0 Bayern Munich 1
             Mandzukic
Manchester City 5 CSKA 2
Aguero (2)             Doumbia (2)
Negredo (3)
 
After 4 games
Bayern Munich 12, Manchester City 9, CSKA Moskva 3, Plzen 0
  
Group E
Chelsea 3 Schalke 0
Eto'o (2)
Ba
Basel 1 Steaua 1
Sio      Piovaccari
  
After 4 games
Chelsea 9, Schalke 6 points, Basel 5, Steaua 2
  
  
Group F
Dortmund 0 Arsenal 1
                    Ramsey
Napoli 3 Marseille 2
Inler        A. Ayew, Thauvin   
Higuain (2)
  
After 4 games
Arsenal 9, Napoli 9, Dortmund 6, Marseille 0
  
  
Group G
Zenit 1 FC Porto 1
Hulk     Lucho González
Atlético 4 Austria Wien 0
Miranda,
Raul Garcia
Filipe Luis, Diego Costa
After 4 games
Atlético 12, Zenit 5, FC Porto 4, Austria Wien 1
  
  
Group H
Ajax 1 Celtic 0
Schöne
Barcelona 3 AC Milan 1
Messi (2)     Piqué
Busquets
  
After 4 games
Barcelona 12, AC Milan 5, Ajax 4, Celtic 3
  
 
Round 3
Group A
Bayer Leverkusen 4 Shakhtar Donetsk 0
Kiessling (2)
Rolfes, Sam
 
Manchester United 1 Real Sociedad 0
Iñigo Rodrihuez (o.g.)
After 3 games
Manchester United 7, Bayer Leverkusen 6, Shakhtar 4 points, Real Sociedad 0
 
Group B
Real Madrid 2 Juventus 1
Ronaldo (2)     Llorente
  
Galatasaray 3 Kobenhavn 1
Felipe Melo   Claudemir
Sneijder
Drogba
 
After 3 games
Real Madrid 9, Galatasaray 4, Juventus 2, Kobenhavn 2,
 
Group C
Anderlecht 0 Paris Saint-Germain 5
                     Ibrahimovic (4), Cavani
 
Benfica 1 Olympiacos 1
Óscar Cardozo   Dominguez
  
After 3 games
PSG 9, Benfica 4,  Olympiacos 4, Anderlecht 0
 
Group D
CSKA Moskva 1 Manchester City 2
Tosic                   Aguero (2)
 
Bayern Munich 5 Plzen 0
Ribéry (2)
Alaba,
Schweinsteiger,
Götze
 
After 3 games
Bayern Munich 9, Manchester City 6, CSKA Moskva 3, Plzen 0
 
Group E 
Steaua Bucharest 1 Basel 1
Leandro Tatu  Diaz
 
Schalke 0 Chelsea 3
                 Torres (2), Hazard
 
After 3 games
Schalke 6 points, Chelsea 6, Basel 4, Steaua 1
 
Group F
Arsenal 1 Dortmund 2
Giroud     Mkhitaryan, R. Lewandowski    
 
Marseille 1 Napoli 2
A.Ayew     Callejón, Zapata
 
After 3 games
Arsenal 6, Dortmund 6, Napoli 6, Marseille 0
 
Group G
FC Porto 0 Zenit Saint Petersburg 1
                  Kerzhakov
 
Austria Wien 0 Atlético Madrid 3
                         Raúl Garcia, Diego Costa 2
After 3 games
Atlético 9, Zenit 4, FC Porto 3, Austria Wien 1
 
Group H
Celtic 2 Ajax 1
Forest    Schöne
Kayal
 
AC Milan 1 Barcelona 1
Robinho    Messi
 
After 3 games
Barcelona 9, AC Milan 5, Celtic 3, Ajax 1
 
Round 2
Group A
Shakhtar Donetsk 1  Manchester United 1
Taison                       Welbeck
 
Bayer Leverkusen 2   Real Sociedad 1
Rolfes                         Vela
Hegeler
 
After 2 games
Shakhtar 4 points, Manchester United 4, Bayer Leverkusen 3, Real Sociedad 0
 
Group B
Real Madrid 4 Kobenhavn 0
Ronaldo (2)
Di Maria (2)
 
Juventus 2   Galatasaray 2
Vidal           Drogba
Quagliarella Umut Mulut
 
After 2 games
Real Madrid 6, Juventus 2, Kobenhavn 1, Galatasaray 1
 
Group C
PSG 3 Benfica 0
Ibrahimovic (2)
Marquinhos
 
Anderlecht 0 Olympiacos 3
                      Mitroglou (3)
 
After 2 games
PSG 6, Benfica 3,  Olympiacos 3, Anderlecht 0
 
Group D
CSKA 3               Plzen 2
Tosic, Honda       Rajtoral, Bakos
Reznik (o.g.)
 
Manchester City 1 Bayern 3
Negredo  Ribéry, T. Müller, Robben
 
After 2 games
Bayern Munich 6, Manchester City 3, CSKA Moskva 3, Plzen 0
 
Group E
 
Basel 0 Schalke 1
             Draxler
 
Steaua 0 Chelsea 4
               Ramires (2), Georgievski (o.g.), Lampard
 
After 2 games
Schalke 6 points, Chelsea 3, Basel 3, Steaua 0
 
Group F
 
Arsenal 2 Napoli 0
Özil, Giroud
 
Dortmund 3 Marseille 0
Lewandowski (2)
Reus
 
After 2 games
Arsenal 6, Dortmund 3, Napoli 3, Marseille 0
 
Group G
 
FC Porto 1 Atlético Mardid 2
Martinez     Godín, Turan
 
Zenit 0 Austria Wien 0
 
After 2 games
Atlético 6, FC Porto 3, Zenit 1, Austria Wien 1
 
Group H
  
Celtic 0 Barcelona 1
              Fabregas
 
Ajax 1 AC Milan 1
Denswil   Balotelli
 
After 2 games
Barcelona 6, AC Milan 4, Ajax 1, Celtic 0
 
First round games
 
Group A 
  
Manchester United (ENG) 4 Bayer Leverkusen (GER) 2
Rooney (2), Van Persie,
Valencia
 
Real Sociedad (SPAIN) 0 Shakhtar Donetsk (UKR) 2
                                           Alex Teixeira (2)
 
Group B 
  
Galatasaray (TUR) 1 Real Madrid (SPAIN) 6
Umit Bulut                 Isco, Benzema (2), C. Ronaldo (3)
 
FC Copenhagen (DEN) 1 Juventus (ITA) 1
N. Jorgensen                     Quagliarella
 
Group C 
  
Benfica (POR) 2 Anderlecht (NETHERLANDS) 0
Djuricic, Luisão
 
Olympiakos (GRE) 1 Paris St-Germain (FRA) 3
Weiss                       Cavani, Thiago Motta (2), Marquinhos
 
Group D
  
Bayern Munich (GER) 3 CSKA Moscow (RUS) 0
Alaba, Mandzukic, Robben
  
Viktoria Plzen (CZE) 0  Manchester City (ENG) 3
                                          Dzeko, Y. Touré, Agüero
 
Group E 
  
Schalke (GER) 3 Steaua Bucharest (ROM) 0
Uchida, Boateng, Drexler
  
Chelsea (ENG) 1  FC Basel (SWI) 2
Oscar                 Salah, Streller
  
Group F 
  
Marseille (FRA) 1 Arsenal (ENG) 2
J. Ayew (pen)        Walcott, Ramsey
  
Napoli (ITA) 2 Borussia Dortmund (GER) 1
Higuain, Insigne   Zuniga (o.g.)
  
Group G 
  
Austria Vienna (AUS) 0 FC Porto (POR) 1
                                          Lucho Gonzalez
  
Atletico Madrid (SPA) 3 Zenit St Petersburg (RUS) 1
Miranda, Arda Turan,    Hulk
Baptista
  
Group H  
  
AC Milan (ITA) 2 Celtic (SCOT) 0
Izaguirre (o.g.)
Muntari
  
Barcelona (SPA) 4 Ajax (NETHERLANDS) 0
Messi (3)
Pique
 
 
 
Next Round: 18/19 February and 25/26 February 2014
Timothy Bancroft-Hinchey
Pravda.Ru
 