Crimea residents to decide their Russian or Ukrainian future on March 16

The Supreme Council of the Crimea has set the date for the referendum about the status of the autonomous republic for March 16th, Crimean Vice Prime Minister Rustam Temirgaleiv told reporters.
There will be two questions on the referendum. "The first one is - do you support the incorporation of the Crimea into the Russian Federation. The second one is - do you support the retrieval of the Constitution of the Crimea from 1992," the official said.
According to the text of the Constitution of the Crimea from 1992, the republic is a part of Ukraine and regulates its relations with Ukraine on the bases of agreements.
 