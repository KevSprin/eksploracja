The plight of the Bedouin in Israel

Destruction of Bedouin villages in Israel, the bulldozing of homes, the denial of rights to Israel's Arab population, the seizing of land, the desecration of Arab cemeteries, the legalization of illegal settlements, the murder of children, disrespect for human rights, all part and parcel of Israel's internal policies.
From the Regional Council of Unrecognized Villages comes the following message after the Israeli Government announced the destruction of two Bedouin villages and the legalization of a Jewish illegal settlement:
It was another bad day for Israel: Racist government decisions, arrests of peaceful protestors, and a child killed in a home demolition.
November 11, 2013. By Yeela Raanan, RCUV.
Yesterday in a festive Government meeting in Sdeh Boker, in conjunction with David Ben Gurion's memorial, prime minister Netanyahu proclaimed the establishment of three new settlements in the Negev: 
 
 
It is amazing that a government can pass such racist decisions... and the media barely thinks this is worth reporting about. And very few Israelis see it as a problem.
A small group of peaceful protestors stood at the junction to Sdeh Boker. But the police decided that they do not like the message of the protestors, and decided to arrest one of us. In protesting the arrest - another two were arrested... Overhearing the conversations between the police officers I heard them coordinate their stories: "Say it was disrupting a police during duty, no, say it was... "
On Friday, a family from the unrecognized Bedouin village of Sawawin, with a demolition order on their home demolished  their home on their own. Many families choose to do this in order to avoid the violence of the police entering their village and in order to avoid the hefty fines that the home owner receives when he receives the "service" of home demolition from the government. A wall that had become unstable with the demolition fell onto two playing children, killing one of them - a 10 year old boy. The second was badly hurt and is in hospital. The death of this child is a result of the harsh living conditions created by the policies of non-recognition, and by the ongoing violence of the home demolitions.
There are 35 Bedouin villages of 1,000 people and more, some dating back hundreds of years. But they have yet to be "legalized"
And the Prawer Law of Displacement is still pursued in the Knesset. Last Wednesday was the first deliberation in the Committee of Internal Affairs. This law, when legislated, will legalize village destructions on a large scale. The three villages affected by the government decisions today will seem only a minor prologue to what we can expect.
"We call upon the Government of Israel to take measures concerning the integration of the Arab-Bedouin community of the Negev into the region based on the principles of partnership, equality, human rights, and a future of prosperity for all the Negev residents" states Dr. Yeela Raanan, Regional Council of Unrecognized Villages
 
Timofei Belov
 