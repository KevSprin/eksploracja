Who will fight Vitali Klitschko?

 
The name of the rival for WBC's heavyweight champion Vitali Klitschko will be announced in the tournament between the four boxers taking the top of the WBC rankings.
"Four boxers from the top of the rankings will hold fights with each other. The results will determine and the mandatory challenger for the title of the WBC world champion, which is owned by Vitali Klitschko," Vladimir Laptev, the head of the Russian delegation said at the conference of the WBC.
These boxers are WBC Silver Bermane Stiverne (Canada), Intercontinental Champion Chris Arreola (USA), WBC Continental Americas champion Deontay Wilder (USA) and USBA champion Bryant Jennings (USA).
After recovering from the trauma of the right hand, Vitali Klitschko said that he was planning to have his next tournament in 2014.
At the same time, the WBC ordered the 42 -year-old athlete to decide before November 30 whether he continues performing in professional boxing or finishes his career. The last time the Ukrainian fought in September 2012 in Moscow, when he knocked out Manuel Charr.
 