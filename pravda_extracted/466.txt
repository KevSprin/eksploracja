Message for International Women's Day 2014

Phumzile Mlambo-Ngcuka United Nations Under-Secretary-General and Executive Director of UN Women
Equality for Women is Progress for All
Today we join the people of the world in celebration of the progress made for women's rights, women's empowerment and gender equality. We also acknowledge that progress has been slow, uneven and in some cases women and girls face new and more complex challenges.
International Women's Day is therefore also a day to recommit ourselves to working harder for gender equality, together as women, men, youth and leaders of nations, communities, religion and commerce.
If we act decisively, with the knowledge that empowering women and girls and supporting their full participation can help solve the greatest challenges of the 21st century, we will find lasting solutions to many of the problems we face in our world. Major challenges such as poverty, inequality, violence against women and girls, and insecurity will be addressed substantially.
Women spend the majority of their income on the well-being of their children and family. Raising women's labour force participation increases economic growth. By ending women's poverty, we will sustainably and significantly reduce extreme poverty worldwide.
By keeping girls in school longer, with quality education we will empower young women to play their full role in society and build stronger families, communities and democracies.
By advancing equal opportunity and removing structural barriers to women's economic empowerment, we will reduce inequality and spur inclusive economic growth.
By supporting women's equal representation in leadership positions in peacemaking, in communities, in politics, in business and in religious institutions, we will build a more just, peaceful and secure world.
By working with men and boys, and doing it together, we will engage humanity in a task that is a responsibility for all.
As we celebrate International Women's Day, we remember and celebrate the women who, led by women in trade unions on this day more than a century ago, called for better working conditions, peace and bread. Their call is still valid today.
Given slow and uneven progress, we continue to call for change. And we pay tribute to the countless women around the world who are making change every day as they go about their daily lives.
Nearly 20 years after the Beijing Women's Conference, and 15 years after the Millennium Summit, we look back with pride at the achievements that have been made. More women are working. More girls are in school. Fewer women die in pregnancy and childbirth. And more women are in leadership positions.
But no country in the world has achieved equality between women and men and girls and boys, and violations of the rights of women and girls are an outrage. So let us build on the lessons learned and the knowledge that equality for women is progress for all, and make greater and bolder progress as we work to achieve the Millennium Development Goals and chart a new post-2015 development agenda.
We can no longer afford to hold back half the world's population. The 21st century has to be different for every woman and girl in the world. She must know that to be born a girl is not the start of a life of hardship and disadvantage. Together we must make sure that:
SHE is Safe and Secure from gender-based violence.
SHE has Human rights that are respected, including reproductive rights.
SHE is Empowered economically and in every way through Education, Equal opportunity, participation and leadership.
This is the SHE Imperative to which I call on you to commit.
Let us all cross the line and stand on the right side of history.
Today and every day, UN Women will stand strong for women's rights, women's empowerment and gender equality.
Equality for women is progress for all.
Related Video - UN Women Executive Director: International Women's Day 2014
 
UN Women
 