Leonardo DiCaprio receives 'Iron Oscar' from Russian provincial theater

 
Leonardo DiCaprio became an honorary actor of the Chelyabinsk Chamber Theater, the public relations director of the cultural institution, Yuri Sychev said.This title will give the Hollywood actor the right to attend all performances of the the theater for free.Employees of the believe that the title should help DiCaprio cope with his grief over the loss of the long-wanted Oscar. This year, Leonardo was nominated for "Best Actor" for his role in "The Wolf of Wall Street."
Actors of the Chelyabinsk theater also expressed a desire to give their colleague an iron Oscar.The spokesman for the theater also said that DiCaprio might be interested in an opportunity to play a role in Chelyabinsk. In particular, Sychev said, the world-famous actor would be very good for a role in "Captive Spirits" , which tells the story of "relations between Alexander Blok, Andrei Bely and the Mendeleevs family."It was also said that the information about the title has been passed to Leonardo DiCaprio through his agents. Nothing is known about his reaction, though.
 
 