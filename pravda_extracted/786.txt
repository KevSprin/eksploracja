North Korea plans space missions to Moon, Mars and asteroids

North Korea announced an intention to launch its own space rocket no later than in 2020. Reportedly, the National Space Committee of the republic approved the revised space program of the country, noting that the country should obtain a worthy place in this area for security purposes.
In June 2020, North Korea plans to launch its own rocket with a 1.5-ton satellite on board. Moreover, the republic plans an unmanned mission to the Moon. In 2040, the country plans missions to Mars and asteroids. To implement the program, the government additionally allocates 410 billion won, or 386 million dollars.
Experts are skeptical about such announcements. Space vehicles need to be tested first, they believe. 
 