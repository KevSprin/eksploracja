USA loses voting right at UNESCO over Palestine

The United Nations deprived the United States of America of the voting right on the issues related to education, science and culture at UNESCO.
This resolution came into effect automatically at the end of the two-year period, after the United States refused to finance the organization. Previously, the US had invested about 80 million dollars, which accounted to 22 percent of the budget of the fund.
In 2011, having stopped the financial support of the organization, the Americans openly expressed their negative position in relation to the inclusion of Palestine in UNESCO's list of members. According to the directive of the U.S. administration, the US was not willing to provide financial assistance to international organizations that took Palestine as a member before the latter entered into a peace treaty with Israel. 
UNESCO Director-General Irina Bokova stated that she saw a decline of the U.S. influence at UNESCO and regretted that, because close cooperation between countries contributed to the early achievement of organization's objectives."
 