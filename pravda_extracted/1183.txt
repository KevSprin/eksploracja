Democracy: 97% of Crimeans want to be part of Russia

In the free and fair referendum conducted today in Crimea, exit polls show that around ninety-seven per cent of Crimea citizens wish to be part of the Russian Federation, against around seven per cent who wish to remain part of the Republic of Ukraine. Turnout: Around 85%.
The questions on the referendum were:
1.      To secede from Ukraine and join the Russian Federation
2.      To remain part of Ukraine and enjoy a greater degree of autonomy
The turnout was eighty-five per cent of the population of Crimea.
Next: The Russian Parliament will vote within the next month on the request of Crimea to join the Russian Federation.
 