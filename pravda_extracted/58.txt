In Amsterdam, drunks work to clean city streets

 
In Amsterdam, local authorities hired drunks for cleaning streets. They will be cleaning city streets for half a packet of tobacco, five cans of beer and 10 euros. The project is aimed at combating anti-social behavior.
"Drunks create a lot of discomfort. They fight, they make noise, they pick up to women. The goal of this project is to get them busy with something and thus get rid of the problems they create," Jerry Holterman, the head of Rainbow Fund said.
The project already has about 20 janitors participating. They were divided into two groups - each working three days a week. One of the participants of the program, a 45-year-old man admitted: "Alcohol is necessary for us to function - this is a drawback of alcoholism as a chronic disease. Many of us have not had any day schedule for years, we have forgotten what it is. Such work is good for us."
 
 