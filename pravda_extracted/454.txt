Pussy Riot story ends with happily ever after

 
A member of punk rock band Pussy Riot, Nadezhda Tolokonnikova, who was kept at the Krasnoyarsk prison hospital, was set free, her husband Pyotr  Verzilov tweeted.
Tolokonnikova, like another member of the group, Maria Alyokhina, who was also released on December 23rd, was pardoned in connection with the 20th anniversary of the Russian Constitution. The State Duma decided to release those convicted of hooliganism.
Pyotr Verzilov wrote on his Twitter that Tolokonnikova intended to facilitate the dismissal of the Chief of the Federal Penitentiary Service in the Republic of Mordovia, General Simchenkov. Tolokonnikova served a part of her sentenced at Correctional Colony No. 14 in Mordovia, where she went on a hunger strike, claiming that the administration of the colony threatened to kill her. She claimed that her human rights were violated and complained of intolerable conditions for prisoners. Tolokonnikova described her claims in an open letter.
Maria Alyokhina, Ekaterina Samutsevich and Nadezhda Tolokonnikova were detained in March 2012. On August 17, 2012, the activists were sentenced to two years in prison for their "punk prayer" at the Cathedral of Christ the Savior. The women were convicted of hooliganism motivated by religious hatred. The activists said they did not intend to offend believers, but wanted to draw people's attention to close ties between the state and the church. On October 10, 2012,  Ekaterina Samutsevich's sentenced was changed to conditional one.
 
 