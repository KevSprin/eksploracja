French satellites spot over 100 fragments of missing Malaysia Airlines Boeing

 
About 122 objects, which can be the fragments of the missing Boeing 777 of Malaysian Airlines, were found in the Indian Ocean. Malaysia's Minister of Transport Hishamuddin Hussein reported on Wednesday that it was French satellites that spotted the wreckage.
The site of the alleged crash is located about 2.5 thousand kilometers to the south-west of the Australian city of Perth. The size of the fragments varies from one to 23 meters in length.
Meanwhile, experts have found that eight minutes after the the aircraft transmitted its last signal to the tracking satellite, Boeing 777 sent another partial signal to the satellite. According to one version, the signal was sent because of fluctuations in the work of on-board electronics.
On March 8, a Boeing 777, flying from Kuala Lumpur to Beijing, went off radar screens. There were 239 people on board. The Malaysian government has officially recognized the missing passengers dead.